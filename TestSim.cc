//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: exampleTestSim.cc 86065 2014-11-07 08:51:15Z gcosmo $
//
/// \file exampleTestSim.cc
/// \brief Main program of the TestSim example

#include "TestSimDetectorConstruction.hh"
#include "TestSimActionInitialization.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "QBBC.hh"
#include "G4OpticalPhysics.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "Randomize.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv)
{
  G4cout << "Hello!" << G4endl;
  G4cout << "argc = " << argc << G4endl;
  // Detect interactive mode (if no arguments) and define UI session
  //
  G4String arg1 = argv[1];
  G4UIExecutive* ui = 0;
  if ((arg1 == "vis.mac")){
    G4cout << ">>> The macro is vis.mac so I'm making the ui" << G4endl;
    ui = new G4UIExecutive(argc, argv);
  }

  G4cout << "I made it past the argc statement. Clearly argc != 2" << G4endl;

  // Choose the Random engine
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  
  // Construct the default run manager
  //
#ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
#else
  G4RunManager* runManager = new G4RunManager;
#endif

  // Set mandatory initialization classes
  //
  G4String geometry;
  G4double distance;
  G4double teflon_reflectivity = 1.;
  G4double mu_reflectivity = 0.;
  G4double box_xy = 69.; //mm
  G4double box_length = 150.; //mm
  G4String device;
  G4String walls;
  G4String led;

  if (argc == 3) {
  	geometry = argv[1];
        distance =  std::strtod(argv[2], nullptr);
  }
  else {
        geometry = argv[2];
        if ((geometry == "box") || (geometry == "box-short")){
            distance =  std::strtod(argv[3], nullptr);
            teflon_reflectivity = std::strtod(argv[4], nullptr);
            mu_reflectivity = std::strtod(argv[5], nullptr);
            box_xy = std::strtod(argv[6], nullptr);
            box_length = std::strtod(argv[7], nullptr);
            device = argv[8];
            walls = argv[9];
            led = argv[10];
            G4cout << ">>> LED is " << led << G4endl;

        }
      }

  // Detector construction
  runManager->SetUserInitialization(new TestSimDetectorConstruction(geometry, distance, 
    teflon_reflectivity, mu_reflectivity, box_xy, box_length, device, walls));

  // Physics list
  G4VModularPhysicsList* physicsList = new QBBC;
  //G4VModularPhysicsList* physicsList = new G4VModularPhysicsList();
  G4OpticalPhysics* opticalPhysics = new G4OpticalPhysics();
  physicsList->RegisterPhysics(opticalPhysics);
  physicsList->SetVerboseLevel(1);
  runManager->SetUserInitialization(physicsList);
    
  // User action initialization
  runManager->SetUserInitialization(new TestSimActionInitialization(box_xy,
                                                                    box_length,
                                                                    teflon_reflectivity,
                                                                    mu_reflectivity,
                                                                    distance,
                                                                    walls,
                                                                    led));
  //runManager->SetUserInitialization(new TestSimActionInitialization(walls));
  
  // Initialize visualization
  //
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  // Process macro or start UI session
  //
  if ( ! ui ) { 
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  }
  else { 
    // interactive mode
    UImanager->ApplyCommand("/control/execute vis.mac");
    ui->SessionStart();
    delete ui;
  }

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted 
  // in the main() program !
  
  delete visManager;
  delete runManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
