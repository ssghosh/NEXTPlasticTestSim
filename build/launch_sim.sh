#!/bin/bash

#tef_reflectivities=(1. 0.99 0.98 0.97 0.96 0.95)
#tef_reflectivities=(0.995 0.99 0.95)
tef_reflectivities=(0)
mu_reflectivities=(0.)
#insertions=(0 36 126 186 216)
insertions=(200)
#insertions=(0 36 66 96 126 156 186 216)
#materials=("TM" "TM_TPB" "TTX" "TTX_TPB") #new
materials=("PTFE") #new
box_xy=${1}
box_length=${2}
led_type=${3} # this can be "blue" or "uv"
#tef_ref=1
echo ${led_type}

for mat in "${materials[@]}"
do
    echo "${mat}"
    for mu_ref in "${mu_reflectivities[@]}"
    do
        for tef_ref in "${tef_reflectivities[@]}"
        do
            for ins in "${insertions[@]}"
            do
                ./TestSim run2.mac box ${ins} ${tef_ref} ${mu_ref} ${box_xy} ${box_length} SiPM ${mat} ${led_type}> box_out.txt
                filename_material="${mat}"
                filename_mu_ref=$(printf %.0f $(bc <<< "100*${mu_ref}"))
                #cp NEXTPlasticTestSim.root ../analysis/box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led_type}.root
                cp box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led_type}.root ../analysis/
                cd ../analysis
                mkdir box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led_type}
                root -l -b -q 'report.C('"\"box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led_type}.root\"",70')' | tee box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led_type}.txt
                cd ../build
            done
        done
    done
done









