#                            NEXT Plastic Test Simulation

This is a simulation of a setup used to test the reflectance of
candidate plastics for the NEXT-100 light tube. 
	
## Setup
Use of this software requires a Unix environment.
You will need to have (Geant4)[http://geant4.web.cern.ch/] and (cmake)[https://cmake.org/] 
installed on your system. Before building the test simulation, run 
```source geant4make.(c)sh``` in your Geant4
build directory to initialize the necessary environment variables.

To get started, clone the repository into the directory containing the 
measurement campaign you wish to analyze, then build with cmake:

```
    $ git clone https://gitlab.com/ssghosh/NEXTPlasticTestSim.git
    $ cd NEXTPlasticTestSim/build
    $ cmake ..
    $ cmake --build .
```

Your ```build``` directory should now contain an executable named ```TestSim```.
In order to run the executable you'll need to pass several command line parameters
to it:

```$ ./TestSim <macro> <geometry> <insertion> <teflon_reflectivity> <mu_reflectivity> <box_xy> <box_length>```

* ```<macro>``` is the name of the macro you want Geant4 to run upon starting the simulation.
This will frequently be the file ```run2.mac```, which simply fires several photons. If you
want to view a visualization of the simulation and interact with it via a command line, 
run ```vis.mac```.
* ```<geometry>``` is one of three things: ```box```, ```box-short```, or ```scintillator```.
All three geometries have the same simulated PMT and LED in them; they differ only in what
the PMT-LED assembly is pointing at.
    - ```box``` creates a plastic box with width and height equal to ```<box_xy>``` and
      length equal to ```<box_length>```, both in mm.
    - ```box-short``` is a special case of ```box``` in which, as the PMT-LED assembly
      is inserted into the box, the box itself is shortened so that the PMT face never
      actually enters it. This option is deprecated in favor of ```box```.
    - ```scintillator``` creates a block of plastic scintillator at a distance ```<insertion>```
      away from the front of the PMT. This was created for an old calibration measurement
      and should not be necessary for most purposes.
* ```<insertion>``` is the distance in mm the PMT-LED assembly should be positioned from the back
of the box (in the case of a box geometry) or from the scintillator face (in the case of the
scintillator geometry). All options beyond this one are ignored in the case of a scintillator
geometry.
* ```<teflon_reflectivity>``` is the reflectivity (in fraction of light reflected) of the plastic
the box is made from.
* ```<mu_reflectivity>``` is the reflectivity (in fraction of light reflected) of the mu metal
tube that surrounds the PMT. In recent iterations of this experiment the mu metal tube
is wrapped in black tape to prevent reflections, so this option should almost always be 0.
* ```<box_xy>``` is the width and height of the plastic box in mm. As the dimensions of the PMT-LED
assembly are hardcoded, the minimum value this can take without colliding with the PMT-LED assembly is 69.
* ```<box_length>``` is the length of the plastic box in mm.

Due to the large number of options required when calling the executable, it is recommended
that you use the script ```launch_sim.sh``` in the ```build``` directory.

```launch_sim.sh``` automates the running of ```TestSim.cc``` for scans through several
values of the input parameters, namely the plastic reflectivity and the insertion of the PMT-LED
assembly into the box. The first few lines of ```launch_sim.sh``` specify lists of values for
these parameters to take. Once you've modified those to your liking, you can run it like so:
```
    $ launch_sim.sh <box_xy> <box_length>
```
Where ```<box_xy>``` and ```<box_length>``` are as described above. 

## Details

### 1- GEOMETRY DEFINITION
	
   The geometry is constructed in the TestSimDetectorConstruction class.
   The setup consists of a plastic box which is open at one end.
 
   The materials are created with the help of the G4NistManager class,
   which allows to build a material from the NIST database using their
   names. All available materials can be found in the Geant4 User's Guide
   for Application Developers, Appendix 10: Geant4 Materials Database.
		
### 4- PRIMARY GENERATOR
  
   The primary generator is defined in the TestSimPrimaryGeneratorAction class.
   The default kinematics is a 6 MeV gamma, randomly distributed in front
   of the envelope across 80% of the transverse (X,Y) envelope size. 
   This default setting can be changed via the Geant4 built-in commands 
   of the G4ParticleGun class.
     
### 5- DETECTOR RESPONSE

   The energy deposited is collected step by step for a selected volume
   in TestSimSteppingAction and accumulated event by event in TestSimEventAction.

   At end of event, the value acummulated in TestSimEventAction is added in 
   TestSimRunAction and summed over the whole run (see 
   TestSimEventAction::EndOfevent()).

### A- VISUALISATION

   The visualization manager is set via the G4VisExecutive class
   in the main() function in exampleTestSim.cc.    
   The initialisation of the drawing is done via a set of /vis/ commands
   in the macro vis.mac. This macro is automatically read from
   the main function when the example is used in interactive running mode.

   By default, vis.mac opens an OpenGL viewer (/vis/open OGL).
   The user can change the initial viewer by commenting out this line
   and instead uncommenting one of the other /vis/open statements, such as
   HepRepFile or DAWNFILE (which produce files that can be viewed with the
   HepRApp and DAWN viewers, respectively).  Note that one can always
   open new viewers at any time from the command line.  For example, if
   you already have a view in, say, an OpenGL window with a name
   "viewer-0", then
      /vis/open DAWNFILE
   then to get the same view
      /vis/viewer/copyView viewer-0
   or to get the same view *plus* scene-modifications
      /vis/viewer/set/all viewer-0
   then to see the result
      /vis/viewer/flush

   The DAWNFILE, HepRepFile drivers are always available
   (since they require no external libraries), but the OGL driver requires
   that the Geant4 libraries have been built with the OpenGL option.

   From Release 9.6 the vis.mac macro in example TestSim has additional commands
   that demonstrate additional functionality of the vis system, such as
   displaying text, axes, scales, date, logo and shows how to change
   viewpoint and style.  Consider copying these to other examples or
   your application.  To see even more commands use help or
   ls or browse the available UI commands in the Application
   Developers Guide, Section 7.1.

   For more information on visualization, including information on how to
   install and run DAWN, OpenGL and HepRApp, see the visualization tutorials,
   for example,
   http://geant4.slac.stanford.edu/Presentations/vis/G4[VIS]Tutorial/G4[VIS]Tutorial.html
   (where [VIS] can be replaced by DAWN, OpenGL and HepRApp)

   The tracks are automatically drawn at the end of each event, accumulated
   for all events and erased at the beginning of the next run.

### B- USER INTERFACES
 
   The user command interface is set via the G4UIExecutive class
   in the main() function in exampleTestSim.cc 
   The selection of the user command interface is then done automatically 
   according to the Geant4 configuration or it can be done explicitly via 
   the third argument of the G4UIExecutive constructor (see exampleB4a.cc). 
