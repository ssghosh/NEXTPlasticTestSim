#include "TestSimOpticalMaterialProperties.hh"
#include <G4MaterialPropertiesTable.hh>
#include <G4PhysicalConstants.hh>

using namespace CLHEP;

G4MaterialPropertiesTable* OpticalMaterialProperties::Air()
{
    G4MaterialPropertiesTable* mpt = new G4MaterialPropertiesTable();

    const G4int entries = 2; G4double energy[entries] = {0.01*eV, 100.*eV};

    // REFRACTIVE INDEX
    G4double rindex[entries] = {1., 1.};
    mpt->AddProperty("RINDEX", energy, rindex, entries);

    // ABSORPTION LENGTH
    G4double abslen[entries] = {1.e8*m, 1.e8*m};
    mpt->AddProperty("ABSLENGTH", energy, abslen, entries); 

    return mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::Teflon(G4double teflon_reflectivity)
{
    G4MaterialPropertiesTable* teflon_mpt = new G4MaterialPropertiesTable();

    const G4int REFL_NUMENTRIES = 2;

    G4double ENERGIES[REFL_NUMENTRIES] = {1.0*eV, 30.*eV};
    G4double RINDEX[REFL_NUMENTRIES] = {1.35, 1.35};
    /// This is for non-coated teflon panels
    //G4double REFLECTIVITY[REFL_NUMENTRIES] = {0.9, 0.9};
    G4double REFLECTIVITY[REFL_NUMENTRIES] = {teflon_reflectivity, teflon_reflectivity};
    G4double specularlobe[REFL_NUMENTRIES] = {0., 0.}; // specular reflection about the normal to a
    //microfacet. Such a vector is chosen according to a gaussian distribution with
    //sigma = SigmaAlhpa (in rad) and centered in the average normal.
    G4double specularspike[REFL_NUMENTRIES] = {0., 0.}; // specular reflection about the average normal
    G4double backscatter[REFL_NUMENTRIES] = {0., 0.}; //180 degrees reflection
    // 1 - the sum of these three last parameters is the percentage of Lambertian reflection

    teflon_mpt->AddProperty("REFLECTIVITY", ENERGIES, REFLECTIVITY, REFL_NUMENTRIES);
    teflon_mpt->AddProperty("RINDEX", ENERGIES, RINDEX, REFL_NUMENTRIES);
    teflon_mpt->AddProperty("SPECULARLOBECONSTANT",ENERGIES,specularlobe,REFL_NUMENTRIES);
    teflon_mpt->AddProperty("SPECULARSPIKECONSTANT",ENERGIES,specularspike,REFL_NUMENTRIES);
    teflon_mpt->AddProperty("BACKSCATTERCONSTANT",ENERGIES,backscatter,REFL_NUMENTRIES);

    return teflon_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::PMT()
{
    G4MaterialPropertiesTable* pmt_mpt = new G4MaterialPropertiesTable();

    const G4int entries = 2;

    G4double ENERGIES[entries] =
    {0.1*eV,100*eV};
    G4double EFFICIENCY[entries] =
    { 1., 1. };
    G4double REFLECTIVITY[entries] =
    { 0., 0. };

    pmt_mpt->AddProperty("EFFICIENCY", ENERGIES, EFFICIENCY, entries);
    pmt_mpt->AddProperty("REFLECTIVITY", ENERGIES, REFLECTIVITY, entries);

    return pmt_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::Foil()
{
    G4MaterialPropertiesTable* foil_mpt = new G4MaterialPropertiesTable();

    const G4int entries = 2;
    G4double energy[entries] = {0.01*eV, 100.*eV};

    G4double reflectivity[entries] = {0.95,0.95};

    foil_mpt->AddProperty("REFLECTIVITY", energy, reflectivity, entries);

    return foil_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::Tape()
{
    G4MaterialPropertiesTable* tape_mpt = new G4MaterialPropertiesTable();

    const G4int entries = 2;
    G4double energy[entries] = {0.01*eV, 100.*eV};

    G4double abs_spec[entries] = {1.,1.};
    G4double em_spec[entries] = {0.,0.};

    tape_mpt->AddProperty("ABSORPTION", energy, abs_spec, entries);
    tape_mpt->AddProperty("EMISSION", energy, em_spec, entries);
    tape_mpt->AddProperty("REFLECTIVITY", energy, em_spec, entries);

    return tape_mpt;
}

// source: refractiveindex.info
G4MaterialPropertiesTable* OpticalMaterialProperties::Copper()
{
    G4MaterialPropertiesTable* copper_mpt = new G4MaterialPropertiesTable();

    const G4int entries = 2;
    //G4double energy[entries] = {2.5*eV, 124.*eV};
    G4double energy[entries] = {0.1*eV, 100.*eV};

    // reflectivity
    G4double reflectivity[entries] = {0., 0.};

    copper_mpt->AddProperty("REFLECTIVITY", energy, reflectivity, entries);

    return copper_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::Mu(G4double mu_reflectivity)
{
    G4MaterialPropertiesTable* mu_mpt = new G4MaterialPropertiesTable();

    const G4int entries = 2;
    //G4double energy[entries] = {2.5*eV, 124.*eV};
    G4double energy[entries] = {0.1*eV, 100.*eV};

    // reflectivity
    //G4double reflectivity[entries] = {0.5, 0.5};
    G4double reflectivity[entries] = {mu_reflectivity, mu_reflectivity};

    mu_mpt->AddProperty("REFLECTIVITY", energy, reflectivity, entries);

    return mu_mpt;
}


G4MaterialPropertiesTable* OpticalMaterialProperties::Scintillator()
{

    G4MaterialPropertiesTable* scint_mpt = new G4MaterialPropertiesTable();

    const G4int ri_entries = 9;
    const G4int wls_numentries = 5;
    const G4int EMISSION_NUMENTRIES = 4;

    G4double ri_energy[ri_entries]
        = {1*eV, 2*eV, 3*eV, 4*eV, 5*eV, 6*eV, 7*eV, 8*eV, 9*eV};
    G4double rindex[ri_entries];
    for (G4int i=0; i<ri_entries; i++) {
        rindex[i] = 1.58;  }
    std::cout << "rindex is " << rindex << std::endl;
    for (G4int i=0; i<ri_entries; i++) {
        std::cout << "rindex " << i << " is " << rindex[i] << std::endl;
        std::cout << "ri_energy " << i << " is " << ri_energy[i] << std::endl;
    }


    /* Right border of the bins*/
    G4double WLS_Emission_Energies[EMISSION_NUMENTRIES] = {
        h_Planck*c_light/(501*nm),
        h_Planck*c_light/(500*nm),
        h_Planck*c_light/(400*nm),
        h_Planck*c_light/(399*nm)};


    G4double Scint_Emission[EMISSION_NUMENTRIES] = {
        0.,
        1.,
        1.,
        0.};


    G4double WLS_Energies[wls_numentries] = {
        h_Planck*c_light/(500*nm),
        h_Planck*c_light/(400*nm),
        h_Planck*c_light/(300*nm), 
        h_Planck*c_light/(250*nm),
        h_Planck*c_light/(200*nm)}; 
    for (G4int i=0; i<wls_numentries; i++) {
        std::cout << "wls_energy " << i << " is " << WLS_Energies[i] << std::endl;
    }

    G4double Scint_WLSAbsLength[wls_numentries] = {10*m, 10*m, 0.1*um, 0.1*um, 0.1*um};
    G4double Scint_AbsLength[wls_numentries] = {5.*cm, 5.*cm, 5.*cm, 5.*cm, 5.*cm};


    scint_mpt->AddProperty("RINDEX", ri_energy, rindex, ri_entries);
    scint_mpt->AddProperty("WLSCOMPONENT", WLS_Emission_Energies, Scint_Emission, EMISSION_NUMENTRIES);
    scint_mpt->AddProperty("WLSABSLENGTH", WLS_Energies, Scint_WLSAbsLength,wls_numentries);
    //scint_mpt->AddProperty("ABSLENGTH", WLS_Energies, Scint_AbsLength,wls_numentries);
    scint_mpt->AddConstProperty("WLSTIMECONSTANT",1.*ns);

    return scint_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::TPB()
{

    G4MaterialPropertiesTable* tpb_mpt = new G4MaterialPropertiesTable();

    const G4int ri_entries  = 9;
    const G4int wls_numentries = 5;
    const G4int EMISSION_NUMENTRIES = 3;

    G4double ri_energy[ri_entries]
        = {1*eV, 2*eV, 3*eV, 4*eV, 5*eV, 6*eV, 7*eV, 8*eV, 9*eV};

    G4double rindex[ri_entries];

    for (int i=0; i<ri_entries; i++) { 
        rindex[i] = 1.635; }

    G4double WLS_Emission_Energies[EMISSION_NUMENTRIES] = {//h_Planck*c_light/(600*nm), 
        h_Planck*c_light/(500*nm),
        h_Planck*c_light/(400*nm)};
    G4double TPB_Emission[EMISSION_NUMENTRIES] = {//1., 
        1., 1.};


    G4double WLS_Energies[wls_numentries] = {h_Planck*c_light/(500*nm), 
        h_Planck*c_light/(400*nm),
        h_Planck*c_light/(300*nm), 
        h_Planck*c_light/(250*nm),
        h_Planck*c_light/(200*nm)}; 
    G4double TPB_WLSAbsLength[wls_numentries] = {10*m, 10*m, 0.1*um, 0.1*um, 0.1*um};
    G4double TPB_AbsLength[wls_numentries] = {1*cm, 1*cm, 10*cm, 10*cm, 10*cm};


    tpb_mpt->AddProperty("RINDEX", ri_energy, rindex, ri_entries);
    tpb_mpt->AddProperty("WLSCOMPONENT", WLS_Emission_Energies,
            TPB_Emission, EMISSION_NUMENTRIES); // done
    tpb_mpt->AddProperty("WLSABSLENGTH", WLS_Energies,
            TPB_WLSAbsLength, wls_numentries);
    //tpb_mpt->AddProperty("ABSLENGTH", WLS_Energies,
    //        TPB_AbsLength, wls_numentries);


    tpb_mpt->AddConstProperty("WLSTIMECONSTANT",1.*ns); // done

    return tpb_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::TTX_TPB()
{
    G4MaterialPropertiesTable* ttx_tpb_mpt = new G4MaterialPropertiesTable();

    const G4int entries  = 32;

    // G4double wavelength[entries] = {290*nm,
    //     300*nm,310*nm,320*nm,330*nm,340*nm,350*nm,360*nm,370*nm,
    //     380*nm,390*nm,400*nm,410*nm,420*nm,430*nm,440*nm,450*nm,
    //     460*nm,470*nm,480*nm,490*nm,500*nm,510*nm,520*nm,530*nm,
    //     540*nm,550*nm,560*nm,570*nm,580*nm,590*nm,600*nm};

    // G4double energies[entries];
    // for (int i=0; i<entries; i++) { 
    //     energies[i] = h_Planck*c_light/(wavelength[i]);};
    G4double energies[entries] = {h_Planck*c_light/(290*nm),h_Planck*c_light/(300*nm),h_Planck*c_light/(310*nm),h_Planck*c_light/(320*nm),h_Planck*c_light/(330*nm),h_Planck*c_light/(340*nm),h_Planck*c_light/(350*nm),h_Planck*c_light/(360*nm),h_Planck*c_light/(370*nm),h_Planck*c_light/(
        380*nm),h_Planck*c_light/(390*nm),h_Planck*c_light/(400*nm),h_Planck*c_light/(410*nm),h_Planck*c_light/(420*nm),h_Planck*c_light/(430*nm),h_Planck*c_light/(440*nm),h_Planck*c_light/(450*nm),h_Planck*c_light/(
        460*nm),h_Planck*c_light/(470*nm),h_Planck*c_light/(480*nm),h_Planck*c_light/(490*nm),h_Planck*c_light/(500*nm),h_Planck*c_light/(510*nm),h_Planck*c_light/(520*nm),h_Planck*c_light/(530*nm),h_Planck*c_light/(
        540*nm),h_Planck*c_light/(550*nm),h_Planck*c_light/(560*nm),h_Planck*c_light/(570*nm),h_Planck*c_light/(580*nm),h_Planck*c_light/(590*nm),h_Planck*c_light/(600*nm)};

    G4double reflectivity[entries] = {
        1.6224,
        1.4196,
        1.3689,
        1.2675,
        1.218,
        1.20785,
        1.218,
        1.2383,
        1.26875,
        1.1165,
        1.06575,
        1.0353,
        0.98455,
        0.96425,
        0.9744,
        0.98455,
        0.98455,
        0.98455,
        0.98455,
        0.98455,
        0.98455,
        0.98455,
        0.9744,
        0.9744,
        0.9744,
        0.9744,
        0.9744,
        0.9744,
        0.9744,
        0.9744,
        0.9744,
        0.9744
        };

    ttx_tpb_mpt->AddProperty("REFLECTIVITY", energies, reflectivity, entries);

    return ttx_tpb_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::TM_TPB()
{
G4MaterialPropertiesTable* tm_tpb_mpt = new G4MaterialPropertiesTable();

const G4int entries  = 32;

    G4double wavelength[entries] = {290*nm,
        300*nm,310*nm,320*nm,330*nm,340*nm,350*nm,360*nm,370*nm,
        380*nm,390*nm,400*nm,410*nm,420*nm,430*nm,440*nm,450*nm,
        460*nm,470*nm,480*nm,490*nm,500*nm,510*nm,520*nm,530*nm,
        540*nm,550*nm,560*nm,570*nm,580*nm,590*nm,600*nm};

    G4double energies[entries];
    for (int i=0; i<entries; i++) { 
        energies[i] = h_Planck*c_light/(wavelength[i]);};

G4double reflectivity[entries] = {
    1.6224,1.3689,1.2675,1.18638,1.16725,1.1368,1.14695,1.16725,1.18755,
    1.06575,1.04545,0.98455,0.9338,0.9338,0.94395,0.94395,0.94395,0.94395,
    0.94395,0.94395,0.94395,0.94395,0.94395,0.94395,0.94395,0.94395,0.94395,
    0.94395,0.94395,0.94395,0.94395,0.94395
    };

tm_tpb_mpt->AddProperty("REFLECTIVITY", energies, reflectivity, entries);

return tm_tpb_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::TTX()
{
G4MaterialPropertiesTable* ttx_mpt = new G4MaterialPropertiesTable();

const G4int entries  = 32;

    G4double wavelength[entries] = {290*nm,
        300*nm,310*nm,320*nm,330*nm,340*nm,350*nm,360*nm,370*nm,
        380*nm,390*nm,400*nm,410*nm,420*nm,430*nm,440*nm,450*nm,
        460*nm,470*nm,480*nm,490*nm,500*nm,510*nm,520*nm,530*nm,
        540*nm,550*nm,560*nm,570*nm,580*nm,590*nm,600*nm};

    G4double energies[entries];
    for (int i=0; i<entries; i++) { 
        energies[i] = h_Planck*c_light/(wavelength[i]);};


G4double reflectivity[entries] = {1.03428,1.03428,1.03428,1.02414,
    1.02515,1.02515,1.015,1.015,1.015,1.015,1.015,1.015,1.015,
    1.015,1.015,1.015,1.015,1.015,1.015,1.015,1.015,1.015,1.015,
    1.015,1.015,1.015,1.015,1.015,1.015,1.015,1.015,1.015,
    };

ttx_mpt->AddProperty("REFLECTIVITY", energies, reflectivity, entries);

return ttx_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::TM()
{
G4MaterialPropertiesTable* tm_mpt = new G4MaterialPropertiesTable();

const G4int entries  = 32;

    G4double wavelength[entries] = {290*nm,
        300*nm,310*nm,320*nm,330*nm,340*nm,350*nm,360*nm,370*nm,
        380*nm,390*nm,400*nm,410*nm,420*nm,430*nm,440*nm,450*nm,
        460*nm,470*nm,480*nm,490*nm,500*nm,510*nm,520*nm,530*nm,
        540*nm,550*nm,560*nm,570*nm,580*nm,590*nm,600*nm};

    G4double energies[entries];
    for (int i=0; i<entries; i++) { 
        energies[i] = h_Planck*c_light/(wavelength[i]);};

G4double reflectivity[entries] = {0.2028,0.1521,0.1521,0.1521,0.15225,
    0.15225,0.15225,0.15225,0.15225,0.01015,0.005075,0.005075,0.005075,
    0.005075,0.005075,0.005075,0.005075,0.005075,0.005075,0.005075,
    0.005075,0.005075,0.005075,0.005075,0.005075,0.005075,
    0.005075,0.005075,0.005075,0.005075,0.005075,0.005075
    };

tm_mpt->AddProperty("REFLECTIVITY", energies, reflectivity, entries);

return tm_mpt;
}

G4MaterialPropertiesTable* OpticalMaterialProperties::SiPM()
{
    G4MaterialPropertiesTable* sipm_mpt = new G4MaterialPropertiesTable();

    const G4int entries  = 23;

    G4double wavelength[entries] = {300*nm,325*nm,
        340*nm,350*nm,375*nm,400*nm,425*nm,450*nm,475*nm,500*nm,
        525*nm,550*nm,575*nm,600*nm,625*nm,650*nm,675*nm,700*nm,
        725*nm,750*nm,775*nm,800*nm,825*nm};

    G4double energies[entries];
    for (int i=0; i<entries; i++) { 
        energies[i] = h_Planck*c_light/(wavelength[i]);};

    //G4double EFFICIENCY[entries] =
    //{ 0.2,0.26,0.29,0.29,0.3,0.365,
    //    0.395,0.405,0.4,0.39,0.37,
    //    0.325,0.3,0.26,0.24,0.2,0.18,
    //    0.15,0.13,0.11,0.1,0.08,0.07};
    G4double EFFICIENCY[entries] =
    { 1.,1.,1.,1.,1.,1.,
        1.,1.,1.,1.,1.,
        1.,1.,1.,1.,1.,1.,
        1.,1.,1.,1.,1.,1.};

    const G4int entries2  = 2;

    G4double energies2[entries2] =
    {0.1*eV,100*eV};

    G4double reflectivity[entries2] =
    { 0., 0. };


    sipm_mpt->AddProperty("REFLECTIVITY", energies2, reflectivity, entries2);
    sipm_mpt->AddProperty("EFFICIENCY", energies, EFFICIENCY, entries);

    return sipm_mpt;
}

