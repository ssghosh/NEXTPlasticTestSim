//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: TestSimPrimaryGeneratorAction.cc 94307 2015-11-11 13:42:46Z gcosmo $
//
/// \file TestSimPrimaryGeneratorAction.cc
/// \brief Implementation of the TestSimPrimaryGeneratorAction class

#include <cmath>
#include <typeinfo>
#include <iomanip>
#include <fstream>
#include <iostream>
#include "TestSimPrimaryGeneratorAction.hh"
#include "TestSimAnalysisManager.hh"

#include "TestSimDetectorConstruction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "Randomize.hh"
#include "LambertianRandomGenerator.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimPrimaryGeneratorAction::TestSimPrimaryGeneratorAction(TestSimAnalysisManager* analysisManager, 
        G4String led)
: G4VUserPrimaryGeneratorAction(),
  fAnalysisManager(analysisManager),
  fParticleGun(0), 
  fCopperThickness(0),
  led(led)
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);
  fCopperTranslation = G4ThreeVector();

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="opticalphoton");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(6.*MeV);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimPrimaryGeneratorAction::~TestSimPrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of ecah event
  //

  // In order to avoid dependence of PrimaryGeneratorAction
  // on DetectorConstruction class we get Envelope volume
  // from G4LogicalVolumeStore.

  
  // set the x and y coordinates of the LED using the mu metal radius
  G4double led_xy = 0.*mm;
  G4double x0 = led_xy;
  G4double y0 = -led_xy;
  G4double z0 = 1*mm; //-13.3
  //double values[] = {1,2,3};
  //double blue_sipm_values[] = {0.02512 , 0.03026 , 0.03664 , 0.04539 , 0.06261 
  //    , 0.08759 ,
  //    0.11393 , 0.14342 , 0.16935 , 0.19788 , 0.22557 , 0.25485 , 0.28476 ,
  //    0.31456 , 0.34179 , 0.36696 , 0.39327 , 0.42034 , 0.44692 , 0.47762 ,
  //    0.50984 , 0.53979 , 0.57216 , 0.60677 , 0.64106 , 0.66983 , 0.69861 ,
  //    0.72378 , 0.74767 , 0.77697 , 0.80354 , 0.83091 , 0.8571 , 0.88112 ,
  //    0.8876 , 0.8727 , 0.85006 , 0.83073 , 0.81604 , 0.80527 , 0.80081 ,
  //    0.80264 , 0.8115 , 0.82686 , 0.84832 , 0.87251 , 0.8969 , 0.92111 ,
  //    0.94617 , 0.96935 , 0.98658 , 0.99512 , 0.995 , 0.98289 , 0.95977 ,
  //    0.93481 , 0.91043 , 0.88576 , 0.86091 , 0.83567 , 0.80981 , 0.78395 , 0.75865 ,
  //    0.73741 , 0.72706 , 0.73013 , 0.7458 , 0.76899 , 0.79316 , 0.79851 , 0.77532 ,
  //    0.74937 , 0.72323 , 0.6988 , 0.67267 , 0.64377 , 0.61472 , 0.58726 , 0.55923 ,
  //    0.53063 , 0.50033 , 0.47223 , 0.44276 , 0.40981 , 0.3761 , 0.34277 , 0.31342 ,
  //    0.2869 , 0.26115 , 0.23596 , 0.20695 , 0.17346 , 0.14315 , 0.11701 , 0.08822 ,
  //    0.06149 , 0.03786 , 0.02462 , 0.01891 , 0.01507 , 0.01194 , 0.00924 , 0.00777};
  //    int datalen = 103;
  double blue_sipm_values[] = {0.02353,
         0.02551, 0.02692, 0.02918, 0.03088, 0.03343, 0.03541, 0.03824, 0.04107, 0.04447, 0.049, 0.05535, 0.06181, 0.07189, 0.08146,
         0.091, 0.0997, 0.11087, 0.12143, 0.13339, 0.14759, 0.16178, 0.17674, 0.19074, 0.20229, 0.21516, 0.2275, 0.23949, 0.2484,
         0.26135, 0.27248, 0.28588, 0.2961, 0.27611, 0.30905, 0.32018, 0.33403, 0.34516, 0.35693, 0.36878, 0.37901, 0.39014, 0.40047,
         0.40956, 0.41864, 0.42773, 0.4371, 0.44732, 0.45754, 0.46805, 0.47827, 0.48906, 0.49985, 0.51007, 0.52098, 0.53211, 0.54358,
         0.55437, 0.56459, 0.57549, 0.58594, 0.59685, 0.60832, 0.61854, 0.62933, 0.63927, 0.64892, 0.66, 0.6768, 0.66795, 0.68754,
         0.69956, 0.71792, 0.70656, 0.73382, 0.74877, 0.72359, 0.76278, 0.77565, 0.78719, 0.79673, 0.80582, 0.81581, 0.82694, 0.83648,
         0.847, 0.85835, 0.86895, 0.87746, 0.88597, 0.88908, 0.88879, 0.88509, 0.87889, 0.87298, 0.86593, 0.8616, 0.85456, 0.85114,
         0.8433, 0.83818, 0.83192, 0.82794, 0.82202, 0.8177, 0.81314, 0.80887, 0.80546, 0.80289, 0.80118, 0.8006, 0.80059, 0.80144,
         0.80313, 0.80596, 0.80965, 0.81418, 0.81872, 0.82462, 0.8312, 0.83946, 0.84765, 0.85673, 0.86581, 0.87432, 0.88486, 0.89329,
         0.90286, 0.91306, 0.92281, 0.93254, 0.94257, 0.95207, 0.96115, 0.96966, 0.97647, 0.98328, 0.98867, 0.99292, 0.99518, 0.99574,
         0.99602, 0.99572, 0.99344, 0.98946, 0.98292, 0.97706, 0.97001, 0.96569, 0.95864, 0.95392, 0.94568, 0.94113, 0.9334, 0.92692,
         0.91858, 0.91011, 0.90406, 0.89821, 0.89186, 0.88282, 0.877, 0.87064, 0.86401, 0.85548, 0.84695, 0.83767, 0.82819, 0.81834,
         0.80906, 0.79883, 0.78954, 0.77912, 0.77022, 0.76226, 0.75322, 0.7474, 0.74217, 0.73739, 0.73193, 0.72851, 0.72623, 0.72622,
         0.72764, 0.73075, 0.73529, 0.74209, 0.74913, 0.75798, 0.76625, 0.77444, 0.78344, 0.7926, 0.79861, 0.80252, 0.79939, 0.79313,
         0.78518, 0.77654, 0.76699, 0.75676, 0.74653, 0.73593, 0.72267, 0.70926, 0.69824, 0.68468, 0.67352, 0.66426, 0.65279, 0.64108,
         0.63091, 0.62211, 0.61302, 0.60393, 0.59455, 0.58574, 0.57665, 0.56728, 0.55762, 0.54796, 0.5383, 0.52893, 0.51899, 0.50905,
         0.49882, 0.48859, 0.47837, 0.46757, 0.45735, 0.44655, 0.43496, 0.42451, 0.41315, 0.40224, 0.39201, 0.38179, 0.37156, 0.36105,
         0.35111, 0.34116, 0.33094, 0.32156, 0.31162, 0.30157, 0.28956, 0.27748, 0.26731, 0.25566, 0.2443, 0.2327, 0.22157, 0.20998,
         0.19969, 0.18823, 0.17543, 0.1656, 0.15471, 0.14145, 0.12933, 0.11721, 0.10641, 0.09581, 0.0852, 0.07542, 0.06588, 0.05769,
         0.04864, 0.0394, 0.03461, 0.02949, 0.0255, 0.02265, 0.02094, 0.01894, 0.01752, 0.01637, 0.01466, 0.0138, 0.01237, 0.01179,
         0.01036, 0.0095, 0.00893, 0.00778};
  int datalen = 285;
  //std::ofstream outputfile;
  //outputfile.open("randvals.txt");
  // instantiate both of our random generators
  CLHEP::RandGeneral blue_theta_gen = CLHEP::RandGeneral(blue_sipm_values,datalen);
  LambertianRandomGenerator lambertianGen;
  //for (int i=0; i<1000000; i++){
  //    double a_value = testing.shoot();
  //    outputfile << a_value << "\n";
  //}
  


  
  // instantiate the values we'll put in the tree
  G4double theta;
  G4double phi;
  // now calculate the momentum components:
  G4ThreeVector generated_direction;
  if (led == "blue") {
      G4double blue_theta = (50.*blue_theta_gen.shoot())-25.; // in degrees
      //theta = blue_theta;
      G4double blue_phi = G4UniformRand()*2.*CLHEP::pi;
      //phi = blue_phi/deg;
      G4double blue_x = std::sin(blue_theta*deg)*std::cos(blue_phi);
      G4double blue_y = std::sin(blue_theta*deg)*std::sin(blue_phi);
      G4double blue_z = std::cos(blue_theta*deg);
      generated_direction = G4ThreeVector(blue_x,blue_y,blue_z);
      theta = std::acos(generated_direction.z())/deg;
      phi = std::acos(generated_direction.x()/std::sin(theta*deg))/deg;
  }
  else if (led == "uv") {
      generated_direction = lambertianGen.GetDirection();
      theta = std::acos(generated_direction.z())/deg;
      phi = std::acos(generated_direction.x()/std::sin(theta*deg))/deg;
  }
  else {
      std::cout << ">>> UNKNOWN LED TYPE SPECIFIED: " << led << std::endl;
  }



  fParticleGun->SetParticleMomentumDirection(generated_direction);
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));
  G4cout << ">>> Photon here. I'm located at (" << x0 << "," << y0 << "," << z0 << ")" << G4endl;

  
  // model the spectrum of the LED
  G4double central_wl;
  G4double wl_spread;
  if (led == "blue") {
      // ThorLabs LED430L
      central_wl = 430.*nm;
      wl_spread = 8.49*nm; // FWHM 20nm
  }
  else if (led == "uv") {
      // ThorLabs LED260W
      central_wl = 260.*nm;
      wl_spread = 5.1*nm; // FWHM 12 nm
  }
  else {
      std::cout << ">>> UNKNOWN LED TYPE SPECIFIED" << std::endl;
  }


  G4double wl = std::abs(G4RandGauss::shoot(central_wl, wl_spread));
  G4double energy = (h_Planck*c_light)/wl;
  std::cout << ">>> the initial photon energy is " << energy << std::endl;

  fParticleGun->SetParticleEnergy(energy);// MeV?

  // Fill branches of analysis tree
  fAnalysisManager->SetPrimaryEnergy(energy/eV);
  fAnalysisManager->SetPrimaryTheta(theta);
  fAnalysisManager->SetPrimaryPhi(phi);

  fParticleGun->GeneratePrimaryVertex(anEvent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

