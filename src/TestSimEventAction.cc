//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: TestSimEventAction.cc 93886 2015-11-03 08:28:26Z gcosmo $
//
/// \file TestSimEventAction.cc
/// \brief Implementation of the TestSimEventAction class

#include <iostream>
#include "TestSimEventAction.hh"
#include "TestSimRunAction.hh"
#include "TestSimAnalysisManager.hh"
#include "TestSimDetectorConstruction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimEventAction::TestSimEventAction(TestSimRunAction* runAction, 
                                       TestSimAnalysisManager* analysisManager)
: G4UserEventAction(),
  fRunAction(runAction),
  fAnalysisManager(analysisManager)
{
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimEventAction::~TestSimEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//void TestSimEventAction::Setcollectorinsertion(G4double d) {
//    fAnalysisManager->Setcollectorinsertion(d); }
void TestSimEventAction::BeginOfEventAction(const G4Event* event)
{
  const TestSimDetectorConstruction* detectorConstruction
      = static_cast<const TestSimDetectorConstruction*>
      (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
  G4double distance = detectorConstruction->GetDistance();
  G4double id = event->GetEventID();
  if (int(id) % 50000 == 0) {
      G4cerr << ">>> At event " << id << G4endl;}
  fAnalysisManager->Setcollectorinsertion(distance);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimEventAction::EndOfEventAction(const G4Event*)
{  
  // Fill analysis tree 
  G4cout << ">>> EventAction is running FillNtuple at end of event" << G4endl;
  fAnalysisManager->FillNtuple();
  G4cout << ">>> EventAction is resetting AnalysisManager at start of event" << G4endl;
  fAnalysisManager->Reset();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimEventAction::SetPrimaryAbsorbed() { 
    std::cout << ">>> SetPrimaryAbsorbed" << std::endl;
    fAnalysisManager->SetPrimaryAbsorbed(); }
void TestSimEventAction::AddSecondaryPhoton() { 
    std::cout << ">>> AddSecondaryPhoton" << std::endl;
    fAnalysisManager->AddSecondaryPhoton(); }
void TestSimEventAction::Firecollector() { 
    std::cout << ">>> Firecollector" << std::endl;
    fAnalysisManager->Firecollector(); }
void TestSimEventAction::Misfirecollector() {
    std::cout << ">>> Misfirecollector" << std::endl;
    fAnalysisManager->Misfirecollector(); }
void TestSimEventAction::Reflectedcollector() {
    std::cout << ">>> Reflectedcollector" << std::endl;
    fAnalysisManager->Reflectedcollector(); }
void TestSimEventAction::CountPlasticReflection() { 
    std::cout << ">>> CountPlasticReflection" << std::endl;
    fAnalysisManager->CountPlasticReflection(); }
void TestSimEventAction::CountPlasticAbsorption() { 
    std::cout << ">>> CountPlasticAbsorption" << std::endl;
    fAnalysisManager->CountPlasticAbsorption(); }
void TestSimEventAction::CountCopperReflection() { 
    std::cout << ">>> CountCopperReflection" << std::endl;
    fAnalysisManager->CountCopperReflection(); }
void TestSimEventAction::CountCopperAbsorption() { 
    std::cout << ">>> CountCopperAbsorption" << std::endl;
    fAnalysisManager->CountCopperAbsorption(); }
void TestSimEventAction::CountEscapedPhoton() { 
    std::cout << ">>> CountEscapedPhoton" << std::endl;
    fAnalysisManager->CountEscapedPhoton(); }
void TestSimEventAction::CountScintillatorAbsorption() { 
    std::cout << ">>> CountScintillatorAbsorption" << std::endl;
    fAnalysisManager->CountScintillatorAbsorption(); }
void TestSimEventAction::CountMuAbsorption() { 
    std::cout << ">>> CountMuAbsorption" << std::endl;
    fAnalysisManager->CountMuAbsorption(); }
void TestSimEventAction::CountFoilAbsorption() { 
    std::cout << ">>> CountFoilAbsorption" << std::endl;
    fAnalysisManager->CountFoilAbsorption(); }
void TestSimEventAction::CountFoilReflection() { 
    std::cout << ">>> CountFoilReflection" << std::endl;
    fAnalysisManager->CountFoilReflection(); }
void TestSimEventAction::FillScintXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(0,xpos,ypos,1.); }
void TestSimEventAction::FillScintExitXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(1,xpos,ypos,1.); }
void TestSimEventAction::FillcollectorHitXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(2,xpos,ypos,1.); }
void TestSimEventAction::FillBoxExitXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(3,xpos,ypos,1.); }
void TestSimEventAction::FillcollectorHitScintXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(4,xpos,ypos,1.); }
void TestSimEventAction::FillMuBlockerXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(5,xpos,ypos,1.); }
void TestSimEventAction::FillEnteringXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(6,xpos,ypos,1.); }
void TestSimEventAction::FillBoxPrimaryExitXY(G4double xpos, G4double ypos) {
    fAnalysisManager->FillHisto(7,xpos,ypos,1.); }
void TestSimEventAction::RecordConversionCoords(std::vector<G4double> coords) {
    fAnalysisManager->RecordConversionCoords(coords); }
