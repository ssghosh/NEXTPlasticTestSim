//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: TestSimSteppingAction.cc 74483 2013-10-09 13:37:06Z gcosmo $
//
/// \file TestSimSteppingAction.cc
/// \brief Implementation of the TestSimSteppingAction class

#include "TestSimSteppingAction.hh"
#include "TestSimEventAction.hh"
#include "TestSimDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include <G4PhysicalConstants.hh>
#include <G4OpBoundaryProcess.hh>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimSteppingAction::TestSimSteppingAction(TestSimEventAction* eventAction)
    : G4UserSteppingAction(),
    fEventAction(eventAction),
    fcollectorvolume(0),
    fCopperVolume(0),
    fPhotonCatcherVolume(0),
    fWorldVolume(0),
    fMuVolume(0),
    fPlasticVolume(0),
    fWLSvolume(0),
    fScintillatorVolume(0),
    fGhostVolume(0),
    fFoilVolume(0),
    fTapeVolume(0),
    fMuBlocker(0),
    geometry(""),
    scint_exit_pos(0,0,0)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimSteppingAction::~TestSimSteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimSteppingAction::UserSteppingAction(const G4Step* step)
{
    // Get parent ID
    G4Track* track = step->GetTrack();
    G4double parent_id;
    if (track) {
        parent_id = track->GetParentID();}
    // Get next volume if there is one
    G4StepPoint* preStep = step->GetPreStepPoint();
    G4StepPoint* pstep = step->GetPostStepPoint();
    G4LogicalVolume* volume_ps = 0;
    G4LogicalVolume* volume_prestep = 0;
    G4ThreeVector ps_position = G4ThreeVector();
    G4ThreeVector ps_momentumdirection = G4ThreeVector();
    G4ThreeVector ps_volumePosition = G4ThreeVector();
    if (pstep) {
        ps_position = pstep->GetPosition();
        ps_momentumdirection = pstep->GetMomentumDirection();
        G4TouchableHandle handle = pstep->GetTouchableHandle();
        G4VPhysicalVolume* physvolume = handle->GetVolume();
        ps_volumePosition = physvolume->GetObjectTranslation();
        volume_ps = physvolume->GetLogicalVolume();
    }
    if (preStep) {
        G4cout << ">>> I'm in the preStep information-gathering branch" << G4endl;
        G4TouchableHandle handle = preStep->GetTouchableHandle();
        G4VPhysicalVolume* physvolume = handle->GetVolume();
        G4cout << "volume_prestep was " << volume_prestep << G4endl;
        volume_prestep = physvolume->GetLogicalVolume();
        G4cout << "volume_prestep is " << volume_prestep << G4endl;
    }


    // Get energy in eV
    G4cout << ">>> Getting energy" << G4endl;
    G4double energy = step->GetPreStepPoint()->GetTotalEnergy()/eV;
    G4double max_collector_energy = (h_Planck*c_light/(300*nm))/eV; //4.13281 eV

    // Get track status
    //G4cout << ">>> Getting track status" << G4endl;
    G4TrackStatus trackStatus = step->GetTrack()->GetTrackStatus();

    const TestSimDetectorConstruction* detectorConstruction
        = static_cast<const TestSimDetectorConstruction*>
        (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    // Get handles for volumes that are common between the two geometries
    if (!(fcollectorvolume && fCopperVolume && fPhotonCatcherVolume)) { 


        if (!fcollectorvolume)
            fcollectorvolume = detectorConstruction->Getcollectorvolume(); 
        if (!fCopperVolume)
            fCopperVolume = detectorConstruction->GetCopperVolume();
        if (!fPhotonCatcherVolume)
            fPhotonCatcherVolume = detectorConstruction->GetPhotonCatcherVolume();
        if (!fMuVolume)
            fMuVolume = detectorConstruction->GetMuVolume();
        if (!fWorldVolume)
            fWorldVolume = detectorConstruction->GetWorldVolume();
    }

    // record things that are common between the two geometries
    // collector fired/misfired
    G4ParticleDefinition *pdef = step->GetTrack()->GetDefinition();
    G4ProcessVector* pv = pdef->GetProcessManager()->GetProcessList();
    G4OpBoundaryProcess *boundary = 0;
    // iterate through process list until you see the optical boundary one
    for (G4int i=0; i<pv->size(); i++) {
        if ((*pv)[i]->GetProcessName() == "OpBoundary") {
            boundary = (G4OpBoundaryProcess*) (*pv)[i];
            break;
        }
    }

    // figure out if it's being detected
    if (pstep->GetStepStatus() == fGeomBoundary 
            && volume_ps == fcollectorvolume 
            && boundary->GetStatus() == Detection
            && ps_momentumdirection.dot(G4ThreeVector(0.,0.,-1.)) > 0.) {

        fEventAction->Firecollector();
        fEventAction->FillcollectorHitXY(ps_position.getX(),ps_position.getY());
    }



    // Absorbed by copper
    if (volume_ps == fCopperVolume && trackStatus == fStopAndKill) {
        //fEventAction->CountCopperAbsorption();
        G4cout << "Photon was absorbed by copper" << G4endl;
    }
    // Reflected by copper
    else if (volume_ps == fCopperVolume && trackStatus != fStopAndKill) {
        fEventAction->CountCopperReflection();
        G4cout << "Photon was reflected by copper" << G4endl;
    }
    //Escapes box
    else if (volume_ps == fPhotonCatcherVolume && trackStatus == fStopAndKill) {
        //fEventAction->CountEscapedPhoton();
        G4cout << "Photon escaped" << G4endl;
    }
    // Absorbed by mu metal
    else if (volume_ps == fMuVolume && trackStatus == fStopAndKill) {
        //fEventAction->CountMuAbsorption();
    }
    else {}

    // now record geometry-specific information 
    if (geometry == "") {
        geometry = detectorConstruction->GetGeometry();}
    // scintillator
    if (geometry == "scintillator") {
        // grab the scintillator-specific volumes
        if (!(fGhostVolume && fScintillatorVolume && fFoilVolume && fTapeVolume)) { 

            if (!fGhostVolume)
                fGhostVolume = detectorConstruction->GetGhostVolume();
            if (!fScintillatorVolume)
                fScintillatorVolume = detectorConstruction->GetScintillatorVolume(); 
            if (!fFoilVolume)
                fFoilVolume = detectorConstruction->GetFoilVolume();
            if (!fTapeVolume)
                fTapeVolume = detectorConstruction->GetTapeVolume();
        }
        // Check for events of interest
        // Incident on the front face of the scintillator
        if (volume_ps == fScintillatorVolume 
                && parent_id == 0  
                && trackStatus == fStopAndKill) {
            G4double xpos = ps_position.getX() - ps_volumePosition.getX();
            G4double ypos = ps_position.getY() - ps_volumePosition.getY();
            fEventAction->FillScintXY(xpos, ypos);
        }
        // Exiting out the front face of the scintillator
        else if (volume_ps == fGhostVolume 
                && parent_id != 0 
                && ps_momentumdirection.dot(G4ThreeVector(0.,0.,-1.)) > 0.) { 
            G4double xpos = ps_position.getX() - ps_volumePosition.getX();
            G4double ypos = ps_position.getY() - ps_volumePosition.getY();
            fEventAction->FillScintExitXY(xpos, ypos);
            scint_exit_pos = G4ThreeVector(xpos,ypos,0);
        }
        // Killed in scintillator
        //else if (volume_ps == fScintillatorVolume && trackStatus == fStopAndKill) {
        //  fEventAction->CountScintillatorAbsorption();
        //}
        // Killed in foil
        else if (volume_ps == fFoilVolume  && trackStatus == fStopAndKill) {
            fEventAction->CountFoilAbsorption();
        }
        // Reflected by foil
        else if (volume_ps == fFoilVolume && trackStatus != fStopAndKill) {
            fEventAction->CountFoilReflection();
        }
        else {}
    }
    else if (geometry == "box") {
        if (!(fGhostVolume && fPlasticVolume && fWLSvolume && fMuBlocker)) { 


            if (!fGhostVolume)
                fGhostVolume = detectorConstruction->GetGhostVolume(); 
            if (!fPlasticVolume)
                fPlasticVolume = detectorConstruction->GetPlasticVolume(); 
            if (!fWLSvolume)
                fWLSvolume = detectorConstruction->GetWLSvolume();
            if (!fMuBlocker)
                fMuBlocker = detectorConstruction->GetMuBlocker();

            G4cout << ">>> fMuBlocker is " << fMuBlocker << G4endl;
        }

        // Check for events of interest
        // converted inside the wavelength shifter
        if (volume_ps == fWLSvolume 
                && parent_id == 0  
                && trackStatus == fStopAndKill) {
            G4double xpos = ps_position.getX();
            G4double ypos = ps_position.getY();
            G4double zpos = ps_position.getZ();
            std::vector<G4double> position = {xpos, ypos, zpos};
            fEventAction->RecordConversionCoords(position);
        }

        if (volume_ps != fWLSvolume 
                && volume_ps != fcollectorvolume
                && volume_ps != fMuVolume
                //&& volume_ps != fPhotonCatcherVolume
                && trackStatus == fStopAndKill) {
            G4cout << ">>> STOP AND KILL OUTSIDE WLS, COLLECTOR, MU IN VOLUME" << G4endl;
        }
        // going toward the opening of the box
        if (volume_ps == fGhostVolume 
                && parent_id != 0 
                && ps_momentumdirection.dot(G4ThreeVector(0.,0.,-1.)) > 0.) { 
            G4double xpos = ps_position.getX(); //- ps_volumePosition.getX();
            G4double ypos = ps_position.getY(); //- ps_volumePosition.getY();
            fEventAction->FillBoxExitXY(xpos, ypos);
        }
        if (volume_ps == fGhostVolume 
                && parent_id == 0 
                && ps_momentumdirection.dot(G4ThreeVector(0.,0.,-1.)) > 0.) { 
            G4double xpos = ps_position.getX(); //- ps_volumePosition.getX();
            G4double ypos = ps_position.getY(); //- ps_volumePosition.getY();
            fEventAction->FillBoxPrimaryExitXY(xpos, ypos);
        }
        // entering the box (from the LED)
        if (volume_ps == fGhostVolume 
                && parent_id == 0 
                && ps_momentumdirection.dot(G4ThreeVector(0.,0.,+1.)) > 0.) { 
            G4double xpos = ps_position.getX(); //- ps_volumePosition.getX();
            G4double ypos = ps_position.getY(); //- ps_volumePosition.getY();
            fEventAction->FillEnteringXY(xpos, ypos);
        }
        if (volume_ps == fMuBlocker) {
            G4cout << ">>> I'M IN THE MU BLOCKER " << G4endl;}
        if (volume_ps == fMuBlocker
                && parent_id != 0
                && ps_momentumdirection.dot(G4ThreeVector(0.,0.,-1.)) > 0.) {
            G4cout << ">>> I'm filling the muBlockerXY histogram" << G4endl;
            G4double xpos = ps_position.getX();
            G4double ypos = ps_position.getY();
            fEventAction->FillMuBlockerXY(xpos, ypos);
        }


        // Reflected by box
        // get the particle definition and list of processes
        G4ParticleDefinition *pdef = step->GetTrack()->GetDefinition();
        G4ProcessVector* pv = pdef->GetProcessManager()->GetProcessList();
        G4OpBoundaryProcess *boundary = 0;
        // iterate through process list until you see the optical boundary one
        for (G4int i=0; i<pv->size(); i++) {
            if ((*pv)[i]->GetProcessName() == "OpBoundary") {
                boundary = (G4OpBoundaryProcess*) (*pv)[i];
                break;
            }
        }

        // figure out if it's being reflected
        G4bool isTeflonReflected = 0;
        if (pstep->GetStepStatus() == fGeomBoundary
                && volume_ps == fPlasticVolume 
                &&  boundary->GetStatus() == LambertianReflection) {
           //         isTeflonReflected = 1;
                    fEventAction->CountPlasticReflection();
        }
        else if (pstep->GetStepStatus() == fGeomBoundary
                 && volume_ps == fPlasticVolume
                 && boundary->GetStatus() == Absorption) {
          //          isTeflonAbsorbed = 1;
                    fEventAction->CountPlasticAbsorption();
        }
        else if (pstep->GetStepStatus() == fGeomBoundary
                 && volume_ps == fMuVolume
                 && boundary->GetStatus() == Absorption) {
         //           isMuAbsorbed = 1;
                    fEventAction->CountMuAbsorption();
        }
        else if (pstep->GetStepStatus() == fGeomBoundary
                 && volume_ps == fPhotonCatcherVolume
                 && boundary->GetStatus() == NoRINDEX) {
        //            isEscaped = 1;
                    fEventAction->CountEscapedPhoton();
        }
        else { }
        if (pstep->GetStepStatus() == fGeomBoundary
                && volume_ps == fPhotonCatcherVolume
                && boundary->GetStatus() != NoRINDEX) {
            G4cout << ">>> PHOTON AT CATCHER WITH BOUNDARY STATUS " << boundary->GetStatus() << G4endl;
        }
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

