//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: TestSimActionInitialization.cc 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file TestSimActionInitialization.cc
/// \brief Implementation of the TestSimActionInitialization class

#include "TestSimActionInitialization.hh"
#include "TestSimPrimaryGeneratorAction.hh"
#include "TestSimRunAction.hh"
#include "TestSimEventAction.hh"
#include "TestSimSteppingAction.hh"
#include "TestSimAnalysisManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//TestSimActionInitialization::TestSimActionInitialization()
TestSimActionInitialization::TestSimActionInitialization(G4double box_xy,
                                                       G4double box_length,
                                                       G4double tef_ref,
                                                       G4double mu_ref,
                                                       G4double ins,
                                                       G4String mat,
                                                       G4String led)
 : G4VUserActionInitialization(), 
    fBoxXY(box_xy),
    fBoxLength(box_length),
    fTeflonReflectivity(tef_ref),
    fMuMetalReflectivity(mu_ref),
    fcollectorinsertion(ins),
    fMaterial(mat),
    led(led)
{
    G4cout << ">>> At TestSimActionInitialization, LED is " << led << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimActionInitialization::~TestSimActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimActionInitialization::BuildForMaster() const
{
  //TestSimAnalysisManager* ana = new TestSimAnalysisManager();
  TestSimAnalysisManager* ana =0;
  TestSimRunAction* runAction = new TestSimRunAction(ana);
  SetUserAction(runAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimActionInitialization::Build() const
{
  // Analysis Manager
//  TestSimAnalysisManager* ana = new TestSimAnalysisManager(walls);
  TestSimAnalysisManager* ana = new TestSimAnalysisManager(fBoxXY,
                                                            fBoxLength,
                                                            fTeflonReflectivity,
                                                            fMuMetalReflectivity,
                                                            fcollectorinsertion,
                                                            fMaterial,
                                                            led);

  // Actions
  SetUserAction(new TestSimPrimaryGeneratorAction(ana, led));
// SetUserAction(new TestSimPrimaryGeneratorAction(ana));

  TestSimRunAction* runAction = new TestSimRunAction(ana);
  SetUserAction(runAction);
  
  TestSimEventAction* eventAction = new TestSimEventAction(runAction, ana);
  SetUserAction(eventAction);
  
  SetUserAction(new TestSimSteppingAction(eventAction));
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
