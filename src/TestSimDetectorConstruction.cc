//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: TestSimDetectorConstruction.cc 94307 2015-11-11 13:42:46Z gcosmo $
//
/// \file TestSimDetectorConstruction.cc
/// \brief Implementation of the TestSimDetectorConstruction class

#include "TestSimDetectorConstruction.hh" 
#include "TestSimOpticalMaterialProperties.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4SystemOfUnits.hh"
#include "G4OpticalSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimDetectorConstruction::TestSimDetectorConstruction(G4String geometry_spec, 
        G4double distance_spec,
        G4double teflon_spec,
        G4double mu_spec,
        G4double box_xy_spec,
        G4double box_length_spec,
        G4String device,
        G4String walls)
: G4VUserDetectorConstruction(),
  fcollectorvolume(0),
  fCopperVolume(0),
  fPhotonCatcherVolume(0),
  fMuVolume(0),
  fGhostVolume(0),
  fScintillatorVolume(0),
  fFoilVolume(0),
  fTapeVolume(0),
  fPlasticVolume(0),
  fWLSvolume(0),
  fMuBlocker(0),
  geometry(geometry_spec),
  distance(distance_spec),
  mu_reflectivity(mu_spec),
  teflon_reflectivity(teflon_spec),
  box_xy(box_xy_spec),
  box_length(box_length_spec),
  device(device),
  walls(walls)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimDetectorConstruction::~TestSimDetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* TestSimDetectorConstruction::Construct()
{
  // order of operations:
  // world
  // collector
  // mu metal lip - add later
  // copper piece
  // photon catcher
  // either teflon box and wls layer or scintillator block
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();


  // Option to switch on/off checking of volumes overlaps
  G4bool checkOverlaps = true;


  // World
  //
  G4double world_sizeXY = 20.*cm;
  G4double world_sizeZ  = 100.*cm;
  //G4double assembly_shift = - 0.1*world_sizeZ;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
  world_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Air());
  G4Material* mu_mat = nist->FindOrBuildMaterial("G4_Fe");
  //mu_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Mu());
  G4Material* Cu_mat = nist->FindOrBuildMaterial("G4_Cu");
  Cu_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Copper());
  
  G4Material* tape_mat = nist->FindOrBuildMaterial("G4_CONCRETE");
  tape_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Tape());


  G4double mu_outer_r = 23.2*mm;
  G4double mu_inner_r = 23.1*mm;
  G4double PMT_radius = 23.*mm;
  G4double PMT_thick = 1*um;


  G4Box* solidWorld =
    new G4Box("World",                       //its name
       0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size

  G4LogicalVolume* logicWorld =
    new G4LogicalVolume(solidWorld,          //its solid
                        world_mat,           //its material
                        "World");            //its name

  G4VPhysicalVolume* physWorld =
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking
    
    G4LogicalVolume* logiccollector;
    //G4double insertion_z = 0;

G4double insertion_z = 0;
if (geometry == "box" || geometry == "box-short")   insertion_z = distance*mm;
  G4cout << ">>> insertion_z is " << insertion_z << G4endl;

G4Material* TPB = nist->FindOrBuildMaterial("G4_TEFLON");
TPB ->SetMaterialPropertiesTable(OpticalMaterialProperties::TTX_TPB());

G4Material* wall_mat;
G4OpticalSurface* boxSurface =  new G4OpticalSurface("boxSurface");
boxSurface->SetType(dielectric_metal);
boxSurface->SetModel(unified);
boxSurface->SetFinish(ground);
boxSurface->SetSigmaAlpha(0.1);



     G4Material* wall_mat_ptfe = nist->FindOrBuildMaterial("G4_TEFLON");
      wall_mat_ptfe->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(teflon_reflectivity));

     G4Material* wall_mat_ttx_tpb = nist->FindOrBuildMaterial("G4_TEFLON");
      wall_mat_ttx_tpb->SetMaterialPropertiesTable(OpticalMaterialProperties::TTX_TPB());
    
     G4Material* wall_mat_tm = nist->FindOrBuildMaterial("G4_TEFLON");
      wall_mat_tm->SetMaterialPropertiesTable(OpticalMaterialProperties::TM());
    
     G4Material* wall_mat_tm_tpb = nist->FindOrBuildMaterial("G4_TEFLON");
      wall_mat_tm_tpb->SetMaterialPropertiesTable(OpticalMaterialProperties::TM_TPB());
     
      G4Material* wall_mat_ttx = nist->FindOrBuildMaterial("G4_TEFLON");
      wall_mat_ttx->SetMaterialPropertiesTable(OpticalMaterialProperties::TTX());
     
      G4Material* wall_mat_tpb = nist->FindOrBuildMaterial("G4_TEFLON");
      wall_mat_tpb->SetMaterialPropertiesTable(OpticalMaterialProperties::TPB());


if (walls == "TPB") {
  wall_mat=wall_mat_tpb;
  boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(1.));}

else if (walls == "PTFE") {
  wall_mat = wall_mat_ptfe;
  boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(teflon_reflectivity));}



else if (walls == "TTX") {
wall_mat = wall_mat_ttx; //else if
boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::TTX());}

else if (walls == "TTX_TPB") {wall_mat = wall_mat_ttx_tpb;  //else if
  boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::TTX_TPB());}

else if (walls == "TM") {wall_mat=wall_mat_tm;  //else if
  boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::TM());}

else if (walls == "TM_TPB") {wall_mat=wall_mat_tm_tpb;  //else if
  boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::TM_TPB());}

else {G4cout << "wall option incorrect! " << wall_mat << G4endl;}

G4cout << ">>> wall_mat is " << wall_mat << G4endl;
G4cout << ">>> box_surface is " << boxSurface << G4endl;

 if (device=="PMT"){

  G4Material* collector_mat = nist->FindOrBuildMaterial("G4_Pb");
  collector_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::PMT());

   // Lead volume for collector
  //

  G4VSolid* solidPMT =
    new G4Tubs("PMT",                       // its name
        0, PMT_radius, PMT_thick/2, 0.0*deg, 360.0*deg);      // its size

  G4LogicalVolume* logicPMT =
    new G4LogicalVolume(solidPMT,           // its solid
                        collector_mat,          // its material
                        "PMT");             // its name

  new G4PVPlacement(0,
                    G4ThreeVector(0, 0, -PMT_thick/2 ),//+ assembly_shift), // face of PMT at (0,0,0)
                    logicPMT,
                    "PMT",
                    logicWorld,
                    false,
                    0,
                    checkOverlaps);

  // Mu metal lip around the PMT
 

  G4double mu_overhang = 0*mm; // how far mu metal overshoots PMT face
  G4double mu_thick = 0.;
  if (geometry == "box") { 
      mu_thick = PMT_thick + mu_overhang+insertion_z; 
  }
  else if (geometry == "box-short") { 
      mu_thick = PMT_thick + mu_overhang;
  }
  else {}

  G4cout << ">>> mu_thick is " << mu_thick << G4endl;

  G4VSolid* fullMu = 
      new G4Tubs("fullMu", 0, mu_outer_r, mu_thick/2, 0.0*deg, 360.0*deg);

  G4VSolid* hollowMu =
      new G4Tubs("hollowMu", 0, mu_inner_r, (mu_thick/2)+1.*mm, 0.0*deg, 360.0*deg);

  G4VSolid* solidMu =
      new G4SubtractionSolid("Mu", fullMu, hollowMu, 0, G4ThreeVector(0,0,0));

  G4LogicalVolume* logicMu = 
      new G4LogicalVolume(solidMu, Cu_mat, "Mu");

  G4VPhysicalVolume* physMu =
    new G4PVPlacement(0,
                    G4ThreeVector(0, 0, -(mu_thick/2) + mu_overhang),
                    logicMu,
                    "Mu",
                    logicWorld,
                    false,
                    0,
                    checkOverlaps);

    // Copper heat sink
  //
  G4double cuXY = 33*mm;
  G4double cuZ = 2.5*mm;

  G4Box* fullCu =
    new G4Box("fullCu",                          // its name
        cuXY/2, cuXY/2, cuZ/2);         // its size

  G4VSolid* holeCu =
    new G4Tubs("holeCu",                       // its name
        0,30*mm, cuZ/2+0.1*mm, 0.0*deg, 360.0*deg);      // its size

  G4VSolid* solidCu =
    new G4SubtractionSolid("Cu", fullCu, holeCu,
        0, G4ThreeVector(cuXY/2, cuXY/2, 0));

  G4LogicalVolume* logicCu =
    new G4LogicalVolume(solidCu,             // its solid
                        Cu_mat,              // its material
                        "Cu");               // its name

  // specify the angle w.r.t. the +x-axis (about the +z-axis)
  // at which the copper piece should be positioned
  G4double rot = 45.;
  G4double offset = 135.; // copper rotation angle
  // build the rotation matrix for the copper piece
  G4RotationMatrix rotm = G4RotationMatrix();
  rotm.rotateZ((rot+offset)*deg);
  G4double cu_radius = 23.34*mm;
  G4double cu_x = cu_radius*std::cos(rot*deg)*mm;
  G4double cu_y = cu_radius*std::sin(rot*deg)*mm;
  G4double cu_mu_dist = 7*mm; // dist from Cu to mu metal lip
  //G4double cu_mu_dist = 0*mm; // dist from Cu to mu metal lip
  G4double cu_z = mu_overhang - cu_mu_dist; //+ assembly_shift;
  G4ThreeVector cu_position = G4ThreeVector(cu_x, cu_y, cu_z);

  fcollectorvolume= logicPMT;
  logiccollector = logicPMT;

  G4Transform3D transform = G4Transform3D(rotm, cu_position);
  fCopperVolume = logicCu;
  fMuVolume = logicMu; 

  if (geometry == "scintillator") {
      G4double int_blockX = 100*mm;
      G4double int_blockY = 100*mm;
      G4double int_blockZ = 10*mm;

      // Scintillator block material parameters
      G4Material* block_mat = nist->FindOrBuildMaterial("G4_TEFLON");
      block_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Scintillator());
      G4Material* foil_mat = nist->FindOrBuildMaterial("G4_Al");
      foil_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Foil());
      G4Material* tape_mat = nist->FindOrBuildMaterial("G4_CONCRETE");
      tape_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Tape());

      // Scintillator block
      //

      G4Box* scintBlock =    
        new G4Box("scintBlock",                         // its name
            0.5*(int_blockX), 0.5*(int_blockY), 
            0.5*(int_blockZ)); //its size

      G4LogicalVolume* scintLogicBlock =                         
        new G4LogicalVolume(scintBlock,            // its solid
                            block_mat,             // its material
                            "block");              // its name

      // we want the center of the scintillator lined up with where the LED is
      G4double hole_radius = cu_radius + (15*mm);
      G4double led_xy = 26.9*mm;
      G4double scint_x0 = led_xy-(0.4*mm);
      G4double scint_y0 = -led_xy+(0.4*mm);
      G4double block_distance = distance*mm; // z distance from edge of mu metal
      G4double scint_z0 = mu_overhang + block_distance + (0.5*int_blockZ); // the face of the block is block_dist from mu lip

      G4VPhysicalVolume* physBlock = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(scint_x0,scint_y0,scint_z0), // centered on where the LED will be
                        scintLogicBlock,                // its logical volume
                        "block",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);
      G4cout << ">>> scint_x0 is " << scint_x0 << G4endl;
      G4cout << ">>> Right side of block is at x = " << scint_x0+(0.5*int_blockX) << G4endl;
      G4cout << ">>> Left side of block is at x = " << scint_x0-(0.5*int_blockX) << G4endl;


      G4Box* sideTape = 
          new G4Box("sideTape",
                  0.5*mm, 0.5*int_blockY, 0.5*int_blockZ);
      G4LogicalVolume* sideLogicTape =
          new G4LogicalVolume(sideTape,
                            tape_mat,
                            "logicSideTape");
      G4VPhysicalVolume* leftSideTape = // left side from LED's perspective
          new G4PVPlacement(0,
                            G4ThreeVector(scint_x0+0.5*int_blockX+0.5*mm,
                                          scint_y0,
                                          scint_z0),
                            sideLogicTape,
                            "leftSideTape",
                            logicWorld,
                            false,
                            0,
                            checkOverlaps);
      G4VPhysicalVolume* rightSideTape = // right side from LED's perspective
          new G4PVPlacement(0,
                            G4ThreeVector(scint_x0-0.5*int_blockX-0.5*mm,
                                          scint_y0,
                                          scint_z0),
                            sideLogicTape,
                            "rightSideTape",
                            logicWorld,
                            false,
                            0,
                            checkOverlaps);

      G4Box* upperLowerTape =
          new G4Box("upperLowerTape",
                  0.5*int_blockX,0.5*mm,0.5*int_blockZ);
      G4LogicalVolume* upperLowerLogicTape = 
          new G4LogicalVolume(upperLowerTape,
                              tape_mat,
                              "upperLowerLogicTape");
      G4VPhysicalVolume* upperTape =
          new G4PVPlacement(0,
                            G4ThreeVector(scint_x0,
                                          scint_y0+0.5*int_blockY+0.5*mm,
                                          scint_z0),
                            upperLowerLogicTape,
                            "upperTape",
                            logicWorld,
                            false,
                            0,
                            checkOverlaps);
      G4VPhysicalVolume* lowerTape =
          new G4PVPlacement(0,
                            G4ThreeVector(scint_x0,
                                          scint_y0+0.5*int_blockY+0.5*mm,
                                          scint_z0),
                            upperLowerLogicTape,
                            "lowerTape",
                            logicWorld,
                            false,
                            0,
                            checkOverlaps);


      // Ghost volume right in front of the scintillator
      //
      G4Box* ghostBox =    
        new G4Box("ghostBox",                         // its name
            0.5*(int_blockX), 0.5*(int_blockY), 
            0.5*mm); //its size

      G4LogicalVolume* ghostLogicBox =                         
        new G4LogicalVolume(ghostBox,            // its solid
                            world_mat,             // its material
                            "ghostBox");              // its name

      G4double ghost_z0 = scint_z0 - 0.5*int_blockZ - 1.5*mm;
      G4VPhysicalVolume* physGhostBox = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(scint_x0,scint_y0,ghost_z0), // centered on where the LED will be
                        ghostLogicBox,                // its logical volume
                        "ghostBox",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);



      // Foil behind the scintillator
      // //
      G4Box* foil = 
          new G4Box("foil",
                  0.5*(int_blockX), 0.5*(int_blockY),
                  0.5*mm);

      G4LogicalVolume* logicFoil =
          new G4LogicalVolume(foil,
                              foil_mat,
                              "foil");

      G4double foil_z = scint_z0 + (0.5*int_blockZ) + (1*mm); // 1 mm behind the scintillator

      G4VPhysicalVolume *physFoil = 
      new G4PVPlacement(0,
                        G4ThreeVector(scint_x0,scint_y0,foil_z), 
                        logicFoil,
                        "foil",
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);

      G4OpticalSurface* foilSurface =  new G4OpticalSurface("foilSurface");
      
      new G4LogicalBorderSurface("foilSurface", physWorld, physFoil, foilSurface);

      // these options are not sacred
      foilSurface->SetType(dielectric_metal);
      foilSurface->SetFinish(polished);
      foilSurface->SetModel(unified); 
      foilSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Foil());
      // Strips of tape in front of the scintillator
      //
      G4double nStrips = 0;
      //G4double stripWidth = 19.5*mm;
      G4double stripWidth = 19.5*mm;
      G4double tapeThick = 0.5*mm;
      if (nStrips) {

          G4Box* tape = 
              new G4Box("tape",
                      0.5*(nStrips*stripWidth), 0.5*(int_blockY),
                      0.5*tapeThick);

          G4LogicalVolume* logicTape = 
              new G4LogicalVolume(tape,
                                  tape_mat,
                                  "tape");

          // want the tape to line up with the right side of the scintillator (looking at it)
          //G4double tape_x = (0.5*nStrips*stripWidth) - (0.5*int_blockX);
          G4double tape_x = scint_x0 + 0.5*int_blockX - 0.5*(nStrips*stripWidth);
          G4double tape_z = scint_z0 - (0.5*int_blockZ)-tapeThick;
          new G4PVPlacement(0,
                            G4ThreeVector(tape_x,scint_y0,tape_z),
                            logicTape,
                            "tape",
                            logicWorld,
                            false,
                            0,
                            checkOverlaps);
          G4cout << ">>> tape_x is " << tape_x << G4endl;
          G4cout << ">>> Right side of tape is x = " << tape_x+(0.5*nStrips*stripWidth) << G4endl;
          G4cout << ">>> Left side of tape is x = " << tape_x-(0.5*nStrips*stripWidth) << G4endl;
          // assign the tape volume handle here
          fTapeVolume = logicTape;
      }
      
      // Ghost volume
      //
      fGhostVolume = ghostLogicBox;
      //fGhostVolume = 0;


      // Scintillator volume
      //
      fScintillatorVolume = scintLogicBlock;
      
      // Foil volume
      //
      fFoilVolume = logicFoil;

      // Tape volume
      //
      // if there's no tape, just return a handle on the foil volume
      if (!nStrips) {
          G4cout << ">>> No tape strips so tapevolume is logicFoil" << G4endl;
          fTapeVolume = logicFoil;}
  }
  else if (geometry == "box" || geometry == "box-short") {
      // Plastic box placement parameters
      //G4double insertion_z = distance*mm; // how far PMT face inserted into the box
      
      G4cout << ">>> here 1" << G4endl;

      G4double box_thick = 5.0*mm;
      G4double int_boxXY = box_xy*mm; 
      G4double int_boxZ = 0.;
      if (geometry == "box") {
        int_boxZ = box_length*mm; // for constant length box
      }
      else if (geometry == "box-short") {
        int_boxZ = box_length*mm - insertion_z; // for shortening box
      }
      else {}
      G4cout << ">>> int_boxZ is " << int_boxZ << G4endl;
      G4cout << ">>> box_length is " << int_boxZ << G4endl;
      
      // black material for escaping photon counter
      G4Material* tape_mat = nist->FindOrBuildMaterial("G4_CONCRETE");
      tape_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Tape());

      // Plastic box material parameters
      //G4Material* box_mat = nist->FindOrBuildMaterial("G4_POLYETHYLENE");
      //box_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(teflon_reflectivity));
      //G4Material* box_mat = nist->FindOrBuildMaterial("G4_POLYETHYLENE");
      //box_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Polyethylene());
      //G4Material* box_mat = nist->FindOrBuildMaterial("G4_POLYPROPYLENE");
      //box_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Polypropylene());
      // Plastic box
      //
      G4Box* fullBox =
        new G4Box("fullBox",                         // its name
            0.5*(int_boxXY + 2*box_thick), 0.5*(int_boxXY + 2*box_thick),
            0.5*((int_boxZ) + box_thick)); //its size

      G4Box* hollow =
        new G4Box("hollow",
            0.5*int_boxXY, 0.5*int_boxXY, 0.5*(int_boxZ));

      G4VSolid* solidBox =
        new G4SubtractionSolid("Box", fullBox, hollow,
            0, G4ThreeVector(0,0,(-box_thick/2)-0.1*um));

      G4LogicalVolume* logicBox =
        new G4LogicalVolume(solidBox,            // its solid
                            wall_mat,             // its material \box_mat
                            "Box");              // its name

      G4ThreeVector boxPos;
      if (geometry == "box") {
          boxPos = G4ThreeVector(0.,0.,0.5*(int_boxZ+box_thick)-insertion_z);
      }
      else if (geometry == "box-short") {
          boxPos = G4ThreeVector(0.,0.,0.5*(int_boxZ+box_thick));
      }
      else {}

      G4VPhysicalVolume* physBox =
        new G4PVPlacement(0,                       // no rotation
                        boxPos,                  // its position
                        logicBox,                // its logical volume
                        "Box",                   // its name
                        logicWorld,              // its mother  volume
                        false,                   // no boolean operation
                        0,                       // copy number
                        checkOverlaps);          // overlaps checking
      G4cout << ">>> Created physBox" << G4endl;


      // Plastic box optical surface
      //G4OpticalSurface* boxSurface =  new G4OpticalSurface("boxSurface");

      //// these options are not sacred
      //boxSurface->SetType(dielectric_metal);
      //boxSurface->SetModel(unified);
      //boxSurface->SetFinish(ground);
      //boxSurface->SetSigmaAlpha(0.1);
      //boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon());


    // (name, logical volume, surface properties)

      // Properties of the wavelength shifter
      G4double wlsThick = 0.005*mm; // 5 um

      //G4double wlsThick = 1*mm; 
   

       // WLS coating test panel
       //
       //G4Box* coating_solid =
       //  new G4Box("DB_WLS_COATING", 70.*mm/2, 70.*mm/2, 0.5*wlsThick);
      
       //G4LogicalVolume* logicWLS =
       //  new G4LogicalVolume(coating_solid, TPB, "DB_WLS_COATING");
      
       //new G4PVPlacement(0, G4ThreeVector(0.,0.,0.5*(int_boxZ + box_thick)+assembly_shift-insertion_z-0.5*(int_boxZ + box_thick)-0.5*wlsThick), logicWLS,
       //      "DB_WLS_COATING", logicWorld, false, 0, checkOverlaps);


      //WLS coating box (INSIDE AIR)
      
      G4Box* wlsFullBox =
        new G4Box("wlsFullBox",                         // its name
            0.5*int_boxXY, 0.5*int_boxXY, 0.5*int_boxZ); //its size

      G4Box* wlsHollow =
        new G4Box("wlsHollow",
            0.5*(int_boxXY-2*wlsThick), 
            0.5*(int_boxXY-2*wlsThick), 
            0.5*(int_boxZ-wlsThick));

      G4VSolid* wlsSolidBox =
        new G4SubtractionSolid("wlsBox", wlsFullBox, wlsHollow,
            0, G4ThreeVector(0,0,(-wlsThick/2)-0.1*um));

      G4LogicalVolume* logicWLSbox =
        new G4LogicalVolume(wlsSolidBox,            // its solid
                            world_mat,             // its material
                            "wlsBox");              // its name

      G4ThreeVector wlsPos(0.,0.,0.);
      if (geometry == "box-short") {
          wlsPos = G4ThreeVector(0.,0.,0.5*(int_boxZ)-100*nm);
      }
      else if (geometry == "box") {
          wlsPos = G4ThreeVector(0.,0.,(0.5*int_boxZ) - insertion_z - 100*nm);
      }
      else {}

      G4VPhysicalVolume* physWLSbox =
      new G4PVPlacement(0,                       // no rotation
                      wlsPos, //+ assembly_shift-insertion_z),
                      logicWLSbox,                // its logical volume
                      "wlsBox",                   // its name
                      logicWorld,              // its mother  volume
                      false,                   // no boolean operation
                      0,                       // copy number
                      checkOverlaps);          // overlaps checking

      // G4OpticalSurface* boxSurface =  new G4OpticalSurface("boxSurface");

      // // these options are not sacred
      // boxSurface->SetType(dielectric_dielectric);
      // boxSurface->SetModel(unified);
      // boxSurface->SetFinish(groundfrontpainted);
      // boxSurface->SetSigmaAlpha(45*deg);
      // boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(teflon_reflectivity));

      new G4LogicalBorderSurface("boxSurface", physWLSbox, physBox, boxSurface);
      
      // Black volume at the entrance of the box 
      //
      G4Box* ghostBox =    
        new G4Box("ghostBox",                         // its name
            0.5*(int_boxXY-(2*wlsThick)), 0.5*(int_boxXY-(2*wlsThick)), 
            0.5*mm); //its size

      G4LogicalVolume* ghostLogicBox =                         
            new G4LogicalVolume(ghostBox,            // its solid
                                world_mat,             // its material
                                "ghostBox");              // its name


      //G4double ghost_z0 = distance*mm+100.0*mm; // right in front of the PMT face
      G4double ghost_z0 = 1.5*mm;
      G4cout << ">>> ghost_z0 is at " << ghost_z0 << G4endl;
      G4VPhysicalVolume* physGhostBox = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(0,0,ghost_z0), // centered on where the LED will be
                        ghostLogicBox,                // its logical volume
                        "ghostBox",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);

      
      // Black volume right behind the PMT 
      //
      G4VSolid* blackVolume =
        new G4Tubs("blackVolume",                       // its name
            0, mu_inner_r, PMT_thick/2, 0.0*deg, 360.0*deg);      // its size

      G4LogicalVolume* blackLogic =                         
        new G4LogicalVolume(blackVolume,            // its solid
                            tape_mat,             // its material
                            "blackVolume");              // its name

      G4double black_z0 = -1.5*um; // right behind the PMT
      G4VPhysicalVolume* physBlackDisk = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(0,0,black_z0), // centered on where the LED will be
                        blackLogic,                // its logical volume
                        "blackVolume",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);
      // plastic volume
      //
      fPlasticVolume = logicBox;
      // WLS volume
      //
      //fWLSvolume = logicWLSbox;
      fWLSvolume = logicWLSbox;
      // Ghost volume
      fGhostVolume = ghostLogicBox;
      
      // mu blocker volume
      fMuBlocker = blackLogic;
  
}
  else {
      G4ExceptionDescription msg;
      msg << "Your geometry option must be either \n";
      msg << "'scintillator' or 'box'. Aborting.";
      G4Exception("TestSimDetectorConstruction::Construct()",
              "UnknownGeometryArgument",FatalErrorInArgument,msg);
  }
 
 // Black volume right behind the PMT 
      //
      G4VSolid* blackVolume =
        new G4Tubs("blackVolume",                       // its name
            0, mu_inner_r, PMT_thick/2, 0.0*deg, 360.0*deg);      // its size

      G4LogicalVolume* blackLogic =                         
        new G4LogicalVolume(blackVolume,            // its solid
                            tape_mat,             // its material
                            "blackVolume");              // its name

      G4double black_z0 = -1.5*um; // right behind the PMT
      G4VPhysicalVolume* physBlackDisk = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(0,0,black_z0), // centered on where the LED will be
                        blackLogic,                // its logical volume
                        "blackVolume",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);

       G4OpticalSurface* phcath_opsur =
    new G4OpticalSurface("PHOTOCATHODE", unified, polished, dielectric_metal);
  phcath_opsur->SetMaterialPropertiesTable(OpticalMaterialProperties::PMT());

  new G4LogicalSkinSurface("PHOTOCATHODE", logiccollector, phcath_opsur);


 
}

else if (device == "SiPM") {

    G4Material* collector_mat = nist->FindOrBuildMaterial("G4_Pb");
    collector_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::SiPM());

    //G4double sipm_square_xy = box_xy; //box_xy/1.42;
    G4double sipm_spacing = 29.83*mm;
    //G4double sipm_tiling = 3*mm+sipm_spacing;
    //G4double sipm_tiles = round(sipm_square_xy/sipm_tiling);
   //////////SiPM
  G4Box* solidSiPM =
    new G4Box("solidSiPM",                         // its name
        0.5*3.*mm, 0.5*3.*mm, 0.5*1.55*mm); //its size


  G4LogicalVolume* logicSiPM =
    new G4LogicalVolume(solidSiPM,            // its solid
                        collector_mat,             // its material
                        "SiPM");              // its name

 //   for (int i=0; i<sipm_tiles; i++) { 
 //     for (int j=0; j<sipm_tiles; j++) {
 //          new G4PVPlacement(0,                       // no rotation
 //                  G4ThreeVector(-round(sipm_square_xy/2)+i*sipm_tiling,-round(sipm_square_xy/2)+j*sipm_tiling,0.), // centered at world center
 //                  logicSiPM,                // its logical volume
 //                  "SiPM",                   // its name
 //                  logicWorld,              // its mother  volume
 //                  false,                   // no boolean operation
 //                  0,                       // copy number
 //                  checkOverlaps);          // overlaps checking}
 //}
  new G4PVPlacement(0,                       // no rotation
                  G4ThreeVector(sipm_spacing,0.,0.), // centered at world center
                  logicSiPM,                // its logical volume
                  "SiPM",                   // its name
                  logicWorld,              // its mother  volume
                  false,                   // no boolean operation
                  0,                       // copy number
                  checkOverlaps);          // overlaps checking

  new G4PVPlacement(0,                       // no rotation
                  G4ThreeVector(0.,sipm_spacing,0.), // centered at world center
                  logicSiPM,                // its logical volume
                  "SiPM",                   // its name
                  logicWorld,              // its mother  volume
                  false,                   // no boolean operation
                  0,                       // copy number
                  checkOverlaps);          // overlaps checking

  new G4PVPlacement(0,                       // no rotation
                  G4ThreeVector(-sipm_spacing,0.,0.), // centered at world center
                  logicSiPM,                // its logical volume
                  "SiPM",                   // its name
                  logicWorld,              // its mother  volume
                  false,                   // no boolean operation
                  0,                       // copy number
                  checkOverlaps);          // overlaps checking

  new G4PVPlacement(0,                       // no rotation
                  G4ThreeVector(0.,-sipm_spacing,0.), // centered at world center
                  logicSiPM,                // its logical volume
                  "SiPM",                   // its name
                  logicWorld,              // its mother  volume
                  false,                   // no boolean operation
                  0,                       // copy number
                  checkOverlaps);          // overlaps checking



  fcollectorvolume= logicSiPM;
  logiccollector = logicSiPM;


  if (geometry == "box" || geometry == "box-short") {
      // Plastic box placement parameters
      //G4double insertion_z = distance*mm; // how far PMT face inserted into the box
      G4double box_thick = 5.0*mm;
      G4double int_boxXY = box_xy*mm; 
      G4double int_boxZ = 0.;
      if (geometry == "box") {
        int_boxZ = box_length*mm; // for constant length box
      }
      else if (geometry == "box-short") {
        int_boxZ = box_length*mm - insertion_z; // for shortening box
      }
      else {}
      G4cout << ">>> int_boxZ is " << int_boxZ << G4endl;
      G4cout << ">>> box_length is " << int_boxZ << G4endl;

     
      // Plastic box material parameters
      //G4Material* box_mat = nist->FindOrBuildMaterial("G4_POLYETHYLENE");
      //box_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(teflon_reflectivity));
      //G4Material* box_mat = nist->FindOrBuildMaterial("G4_POLYETHYLENE");
      //box_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Polyethylene());
      //G4Material* box_mat = nist->FindOrBuildMaterial("G4_POLYPROPYLENE");
      //box_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::Polypropylene());
      // Plastic box
      //
      G4Box* fullBox =
        new G4Box("fullBox",                         // its name
            0.5*(int_boxXY + 2*box_thick), 0.5*(int_boxXY + 2*box_thick),
            0.5*((int_boxZ) + box_thick)); //its size

      G4Box* hollow =
        new G4Box("hollow",
            0.5*int_boxXY, 0.5*int_boxXY, 0.5*(int_boxZ));

      G4VSolid* solidBox =
        new G4SubtractionSolid("Box", fullBox, hollow,
            0, G4ThreeVector(0,0,(-box_thick/2)-0.1*um));

      G4LogicalVolume* logicBox =
        new G4LogicalVolume(solidBox,            // its solid
                            wall_mat,             // its material \\wall_mat
                            "Box");              // its name

      G4ThreeVector boxPos;
      if (geometry == "box") {
          boxPos = G4ThreeVector(0.,0.,0.5*(int_boxZ+box_thick)-insertion_z);
      }
      else if (geometry == "box-short") {
          boxPos = G4ThreeVector(0.,0.,0.5*(int_boxZ+box_thick));
      }
      else {}

      G4VPhysicalVolume* physBox =
        new G4PVPlacement(0,                       // no rotation
                        boxPos,                  // its position
                        logicBox,                // its logical volume
                        "Box",                   // its name
                        logicWorld,              // its mother  volume
                        false,                   // no boolean operation
                        0,                       // copy number
                        checkOverlaps);          // overlaps checking
      G4cout << ">>> I've created the physical box" << G4endl;


      // Plastic box optical surface
      //G4OpticalSurface* boxSurface =  new G4OpticalSurface("boxSurface");

      //// these options are not sacred
      //boxSurface->SetType(dielectric_metal);
      //boxSurface->SetModel(unified);
      //boxSurface->SetFinish(ground);
      //boxSurface->SetSigmaAlpha(0.1);
      //boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon());


    // (name, logical volume, surface properties)

      // Properties of the wavelength shifter
      G4double wlsThick = 0.005*mm; // 5 um
      G4cout << ">>> wlsThick is " << wlsThick << G4endl;
      //G4double wlsThick = 1*mm; 

       // WLS coating test panel
       //
       //G4Box* coating_solid =
       //  new G4Box("DB_WLS_COATING", 70.*mm/2, 70.*mm/2, 0.5*wlsThick);
      
       //G4LogicalVolume* logicWLS =
       //  new G4LogicalVolume(coating_solid, TPB, "DB_WLS_COATING");
      
       //new G4PVPlacement(0, G4ThreeVector(0.,0.,0.5*(int_boxZ + box_thick)+assembly_shift-insertion_z-0.5*(int_boxZ + box_thick)-0.5*wlsThick), logicWLS,
       //      "DB_WLS_COATING", logicWorld, false, 0, checkOverlaps);


      //WLS coating box (INSIDE AIR)
      
      G4Box* wlsFullBox =
        new G4Box("wlsFullBox",                         // its name
            0.5*int_boxXY, 0.5*int_boxXY, 0.5*int_boxZ); //its size

      G4Box* wlsHollow =
        new G4Box("wlsHollow",
            0.5*(int_boxXY-2*wlsThick), 
            0.5*(int_boxXY-2*wlsThick), 
            0.5*(int_boxZ-wlsThick));

      G4VSolid* wlsSolidBox =
        new G4SubtractionSolid("wlsBox", wlsFullBox, wlsHollow,
            0, G4ThreeVector(0,0,(-wlsThick/2)-0.1*um));

      G4LogicalVolume* logicWLSbox =
        new G4LogicalVolume(wlsSolidBox,            // its solid
                            world_mat,             // its material
                            "wlsBox");              // its name

      G4ThreeVector wlsPos(0.,0.,0.);
      if (geometry == "box-short") {
          wlsPos = G4ThreeVector(0.,0.,0.5*(int_boxZ)-100*nm);
      }
      else if (geometry == "box") {
          wlsPos = G4ThreeVector(0.,0.,(0.5*int_boxZ) - insertion_z - 100*nm);
      }
      else {}

      G4VPhysicalVolume* physWLSbox =
      new G4PVPlacement(0,                       // no rotation
                      wlsPos, //+ assembly_shift-insertion_z),
                      logicWLSbox,                // its logical volume
                      "wlsBox",                   // its name
                      logicWorld,              // its mother  volume
                      false,                   // no boolean operation
                      0,                       // copy number
                      checkOverlaps);          // overlaps checking
      G4cout << ">>> Created physWLSbox" << G4endl;

      // G4OpticalSurface* boxSurface =  new G4OpticalSurface("boxSurface");

      // // these options are not sacred
      // boxSurface->SetType(dielectric_dielectric);
      // boxSurface->SetModel(unified);
      // boxSurface->SetFinish(groundfrontpainted);
      // boxSurface->SetSigmaAlpha(45*deg);
      // boxSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Teflon(teflon_reflectivity));

      new G4LogicalBorderSurface("boxSurface", physWLSbox, physBox, boxSurface);
      
      // Black volume at the entrance of the box 
      
      G4Box* ghostBox =    
        new G4Box("ghostBox",                         // its name
            0.5*(int_boxXY-(2*wlsThick)), 0.5*(int_boxXY-(2*wlsThick)), 
            0.5*mm); //its size
      //G4Box* ghostBox =    
      //  new G4Box("ghostBox",                         // its name
      //      (int_boxXY-(2*wlsThick)), (int_boxXY-(2*wlsThick)), 
      //      0.5*mm); //its size
      G4cout << ">>> SiPM branch ghost box XY is " << (int_boxXY-(2*wlsThick)) << G4endl;

      G4LogicalVolume* ghostLogicBox =                         
            new G4LogicalVolume(ghostBox,            // its solid
                                world_mat,             // its material
                                "ghostBox");              // its name


      G4double ghost_z0 = 10*mm; // right in front of the SiPM face
      G4cout << ">>> ghost_z0 is " << ghost_z0 << G4endl;
      G4VPhysicalVolume* physGhostBox = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(0,0,ghost_z0), // centered on where the LED will be
                        ghostLogicBox,                // its logical volume
                        "ghostBox",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);


       //Black volume right behind the SiPM 
      

      G4double blackXY = 6.5*cm;
      G4VSolid* blackVolume =
        new G4Box("blackVolume",                       // its name
            0.5*(blackXY), 0.5*(blackXY),  0.5*mm); //its size

      G4LogicalVolume* blackLogic =                         
        new G4LogicalVolume(blackVolume,            // its solid
                            tape_mat,             // its material
                            "blackVolume");              // its name

      G4double black_z0 = -3*mm; // right behind the SiPM
      G4VPhysicalVolume* physBlackDisk = 
      new G4PVPlacement(0,                       // no rotation
                        G4ThreeVector(0,0,black_z0), // centered on where the LED will be
                        blackLogic,                // its logical volume
                        "blackVolume",                   // its name
                        logicWorld,
                        false,
                        0,
                        checkOverlaps);
      fMuBlocker = blackLogic;
      
            // plastic volume
      //
      fPlasticVolume = logicBox;
      // WLS volume
      //
      //fWLSvolume = logicWLSbox;
      fWLSvolume = logicWLSbox;
      // Ghost volume
      fGhostVolume = ghostLogicBox;
      
      // mu blocker volume
      G4double led_xy = 26.9*mm;
      
  }
  else {
      G4ExceptionDescription msg;
      msg << "Your geometry option must be either \n";
      msg << "'scintillator' or 'box'. Aborting.";
      G4Exception("TestSimDetectorConstruction::Construct()",
              "UnknownGeometryArgument",FatalErrorInArgument,msg);
  }

 G4OpticalSurface* phcath_opsur =
    new G4OpticalSurface("PHOTOCATHODE", unified, polished, dielectric_metal);
  phcath_opsur->SetMaterialPropertiesTable(OpticalMaterialProperties::PMT());

  new G4LogicalSkinSurface("PHOTOCATHODE", logiccollector, phcath_opsur);


    }

  // PMT surface

 

  
  //Mu metal optical surface

  //G4OpticalSurface* muSurface =  new G4OpticalSurface("muSurface");

  //new G4LogicalBorderSurface("muSurface", physWorld, physMu, muSurface);

  // these options are not sacred
  // we'll give it the same optical properties as copper since it's really
  // just reflecting
  //muSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Mu(mu_reflectivity));
  //muSurface->SetType(dielectric_metal);
  //muSurface->SetFinish(ground);
  //muSurface->SetModel(unified); 
  //cuSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Copper());


  // create the overall transform

  //G4VPhysicalVolume* physCu =
  //new G4PVPlacement(transform,               // rotation and position
  //                  logicCu,                 // its logical volume
  //                  "Cu",                    // its name
  //                  logicWorld,                // its mother  volume
  //                  false,                   // no boolean operation
  //                  0,                       // copy number
  //                  checkOverlaps);          // overlaps checking

  ////Copper mount optical surface

  //G4OpticalSurface* cuSurface =  new G4OpticalSurface("cuSurface");

  //new G4LogicalBorderSurface("cuSurface", physWorld, physCu, cuSurface);

  //// these options are not sacred
  //cuSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Copper());
  //cuSurface->SetType(dielectric_metal);
  //cuSurface->SetFinish(ground);
  //cuSurface->SetModel(unified); 
  //cuSurface->SetMaterialPropertiesTable(OpticalMaterialProperties::Copper());
  
  // Photon catcher

  G4Material* collector_mat = nist->FindOrBuildMaterial("G4_Pb");
  collector_mat->SetMaterialPropertiesTable(OpticalMaterialProperties::SiPM());
  //
  G4Box* fullBox2 =
    new G4Box("fullBox2",                         // its name
        0.99*0.5*world_sizeXY, 0.99*0.5*world_sizeXY, 0.99*0.5*world_sizeZ); //its size

  G4Box* hollow2 =
    new G4Box("hollow2",
        0.98*0.5*world_sizeXY, 0.98*0.5*world_sizeXY, 0.98*0.5*world_sizeZ);

  G4VSolid* solidCatcher =
    new G4SubtractionSolid("Catcher", fullBox2, hollow2,
        0, G4ThreeVector(0,0,0));

  G4LogicalVolume* logicCatcher =
    new G4LogicalVolume(solidCatcher,            // its solid
                        collector_mat,             // its material
                        "Catcher");              // its name

  new G4PVPlacement(0,                       // no rotation
                  G4ThreeVector(0.,0.,0.), // centered at world center
                  logicCatcher,                // its logical volume
                  "Catcher",                   // its name
                  logicWorld,              // its mother  volume
                  false,                   // no boolean operation
                  0,                       // copy number
                  checkOverlaps);          // overlaps checking


  // Set handles for common volumes
  //

  // Photon catcher volume
  //
  fPhotonCatcherVolume = logicCatcher;

  // Mu metal volume
  //

  // World volume
  //
  fWorldVolume = logicWorld;
  
  //always return the physical World
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
