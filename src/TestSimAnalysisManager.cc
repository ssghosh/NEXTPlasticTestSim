//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file analysis/AnaEx01/src/TestSimAnalysisManager.cc
/// \brief Implementation of the TestSimAnalysisManager class
//
//
// $Id: TestSimAnalysisManager.cc 105494 2018-08-23 09:02:56Z $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 

#include "TestSimAnalysisManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimAnalysisManager::TestSimAnalysisManager(G4double box_xy,
                                               G4double box_length,
                                               G4double tef_ref,
                                               G4double mu_ref,
                                               G4double ins,
                                               G4String mat,
                                               G4String led)
 :  fFactoryOn(false),
    fBoxXY(box_xy),
    fBoxLength(box_length),
    fTeflonReflectivity(tef_ref),
    fcollectorinsertion(ins),
    fMaterial(mat),
    fLED(led)
{
  //fcollectorinsertion = 0.;
  fCopperRotation = 0.;
  fPrimaryEnergy = 0.;
  fPrimaryTheta = 0.;
  fPrimaryPhi = 0.;
  fWLSabsorbed = 0;
  fNemitted = 0.;
  fcollectorhits = 0.;
  fcollectorduds = 0.;
  fPlasticReflections = 0.;
  fPlasticAbsorptions = 0.;
  fCopperReflections = 0.;
  fCopperAbsorptions = 0.;
  fEscapedPhotons = 0;
  fConversionCoords = {0,0,0};
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestSimAnalysisManager::~TestSimAnalysisManager()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimAnalysisManager::Book()
{
  // Create or get analysis manager
  // The choice of analysis technology is done via selection of a namespace
  // in TestSimAnalysisManager.hh
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);
      
  // Create directories 
  analysisManager->SetHistoDirectoryName("histo");
  analysisManager->SetNtupleDirectoryName("ntuples");
    
  // Open an output file
  //
  // convert the elements of the output filename into strings
  G4String strBoxXY = std::to_string(fBoxXY);
  strBoxXY.erase(strBoxXY.find_last_not_of('0')+1,std::string::npos);
  strBoxXY.erase(strBoxXY.find_last_not_of('.')+1,std::string::npos);

  G4String strBoxLength = std::to_string(fBoxLength);
  strBoxLength.erase(strBoxLength.find_last_not_of('0')+1,std::string::npos);
  strBoxLength.erase(strBoxLength.find_last_not_of('.')+1,std::string::npos);

  G4String strTeflonReflectivity = std::to_string(fTeflonReflectivity);
  strTeflonReflectivity.erase(strTeflonReflectivity.find_last_not_of('0')+1,std::string::npos);
  strTeflonReflectivity.erase(strTeflonReflectivity.find_last_not_of('.')+1,std::string::npos);

  G4String strMuMetalReflectivity = std::to_string(fMuMetalReflectivity);
  strMuMetalReflectivity.erase(strMuMetalReflectivity.find_last_not_of('0')+1,std::string::npos);
  strMuMetalReflectivity.erase(strMuMetalReflectivity.find_last_not_of('.')+1,std::string::npos);

  G4String strcollectorinsertion = std::to_string(fcollectorinsertion);
  strcollectorinsertion.erase(strcollectorinsertion.find_last_not_of('0')+1,std::string::npos);
  strcollectorinsertion.erase(strcollectorinsertion.find_last_not_of('.')+1,std::string::npos);

  G4String outputname = "box"+strBoxXY+"x"+strBoxLength
      +"_"+"tef"+strTeflonReflectivity+"_mu"+strMuMetalReflectivity+"_ins"
      +strcollectorinsertion+"_mat"+fMaterial+"_led"+fLED+".root";
  G4cout << ">>> analysismanager outputname = " << outputname << G4endl;
  //G4bool fileOpen = analysisManager->OpenFile("NEXTPlasticTestSim");
  G4bool fileOpen = analysisManager->OpenFile(outputname);
  if (! fileOpen) {
    G4cerr << "\n---> TestSimAnalysisManager::Book(): cannot open "
           << analysisManager->GetFileName() << G4endl;
    return;
  }
  
  // id = 0
  analysisManager->CreateH2("scintXY", 
                            "(x,y) of photons incident on scintillator (mm)", 
                            1000, -60.*mm, +60.*mm, 
                            1000, -60.*mm, +60.*mm);

  // id = 1
  analysisManager->CreateH2("scintExitXY", 
                            "(x,y) of photons exiting scintillator (mm)", 
                            1000, -60.*mm, +60.*mm, 
                            1000, -60.*mm, +60.*mm);

  // id = 2
  analysisManager->CreateH2("collectorHitXY", 
                          "(x,y) of blue photons hitting collector (mm)", 
                            //device,
                            100, -32.5*mm, +32.5*mm, 
                            100, -32.5*mm, +32.5*mm);
  
  // id = 3
  analysisManager->CreateH2("ExitXY", 
                            "(x,y) of photons exiting box", 
                            1000, -40.*mm, +40.*mm, 
                            1000, -40.*mm, +40.*mm);
  
  // id = 4
  analysisManager->CreateH2("collectorHitScintXY", 
                            "(x,y) of detected photons at scintillator", 
                            1000, -60.*mm, +60.*mm, 
                            1000, -60.*mm, +60.*mm);
  
  // id = 5
  analysisManager->CreateH2("MuBlockerXY", 
                            "(x,y) of photons entering mu tube and missing collector", 
                            500, -40.*mm, +40.*mm, 
                            500, -40.*mm, +40.*mm);

  // id = 6
  analysisManager->CreateH2("EnteringXY", 
                            "(x,y) of photons entering box",
                            500, -40.*mm, +40.*mm, 
                            500, -40.*mm, +40.*mm);
  
  // id = 7
  analysisManager->CreateH2("ExitPrimaryXY", 
                            "(x,y) of primary photons exiting box",
                            500, -40.*mm, +40.*mm, 
                            500, -40.*mm, +40.*mm);
  
  // id = 8
  analysisManager->CreateH2("ReturningPrimaryXY", 
                            "(x,y) of primary photons returning into box",
                            500, -40.*mm, +40.*mm, 
                            500, -40.*mm, +40.*mm);

  // Create ntuples.
  // Ntuples ids are generated automatically starting from 0.

  // Create 1st ntuple (id = 0)
  analysisManager->CreateNtuple("ntuple", "BoxEvents");
  analysisManager->CreateNtupleDColumn("collectorinsertion"); // column id = 0
  analysisManager->CreateNtupleDColumn("CopperRotation"); // column id = 1
  analysisManager->CreateNtupleDColumn("PrimaryEnergy"); // column Id = 2
  analysisManager->CreateNtupleDColumn("PrimaryTheta"); // column Id = 3
  analysisManager->CreateNtupleDColumn("PrimaryPhi"); // id = 4
  analysisManager->CreateNtupleIColumn("WLSabsorbed"); // id = 5
  analysisManager->CreateNtupleIColumn("Nemitted");  // id = 6
  analysisManager->CreateNtupleDColumn("collectorhits"); // id = 7
  analysisManager->CreateNtupleDColumn("collectorduds"); // id = 8
  //analysisManager->CreateNtupleDColumn(walls); // id = 9
  analysisManager->CreateNtupleDColumn("PlasticReflections"); // id = 9
  analysisManager->CreateNtupleDColumn("PlasticAbsorptions"); // id = 10
  analysisManager->CreateNtupleDColumn("CopperReflections"); // id = 11
  analysisManager->CreateNtupleDColumn("CopperAbsorptions"); // id = 12
  analysisManager->CreateNtupleDColumn("EscapedPhotons"); // id = 13
  analysisManager->CreateNtupleDColumn("ScintillatorAbsorbed"); // id = 14
  analysisManager->CreateNtupleDColumn("MuAbsorbed"); // id = 15
  analysisManager->CreateNtupleDColumn("FoilAbsorbed"); // id = 16
  analysisManager->CreateNtupleDColumn("FoilReflections"); // id = 17
  analysisManager->CreateNtupleDColumn("collectorreflections"); // id = 18
  analysisManager->CreateNtupleDColumn("XConversionCoord"); // id = 19
  analysisManager->CreateNtupleDColumn("YConversionCoord"); // id = 20
  analysisManager->CreateNtupleDColumn("ZConversionCoord"); // id = 21
  analysisManager->FinishNtuple();

  fFactoryOn = true;

  G4cout << "\n----> Output file is open in " 
         << analysisManager->GetFileName() << "." 
         << analysisManager->GetFileType() << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimAnalysisManager::Save()
{
  if (! fFactoryOn) return;

  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();
   
  G4cout << "\n----> Ntuples are saved\n" << G4endl;
      
  delete G4AnalysisManager::Instance();
  fFactoryOn = false;
}

void TestSimAnalysisManager::FillHisto(G4int id, G4double xbin, G4double ybin, G4double weight)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->FillH2(id, xbin, ybin, weight);
}

void TestSimAnalysisManager::Normalize(G4int id, G4double fac)
{
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    G4H2* h2 = analysisManager->GetH2(id);
    if (h2) h2->scale(fac);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestSimAnalysisManager::FillNtuple()
{
  G4cout << ">>> At FillNtuple fPrimaryTheta = " << fPrimaryTheta << G4endl;
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  // Fill 1st ntuple ( id = 0)
  analysisManager->FillNtupleDColumn(0, 0, fcollectorinsertion); // Separation of collector face with box opening
  analysisManager->FillNtupleDColumn(0, 1, fCopperRotation); // Rotation of copper piece around collector axis
  analysisManager->FillNtupleDColumn(0, 2, fPrimaryEnergy); // Energy of initial VUV photon (eV)
  analysisManager->FillNtupleDColumn(0, 3, fPrimaryTheta); // Theta of initial photon
  analysisManager->FillNtupleDColumn(0, 4, fPrimaryPhi); // Phi of initial photon
  analysisManager->FillNtupleIColumn(0, 5, fWLSabsorbed); // 0 or 1 flag for initial photon's
                                                         // being absorbed by the WLS
  analysisManager->FillNtupleIColumn(0, 6, fNemitted); // Number of photons emitted by the WLS
  analysisManager->FillNtupleDColumn(0, 7, fcollectorhits); // Number of collector hits (counted photons)
  analysisManager->FillNtupleDColumn(0, 8, fcollectorduds); // Number of photons incident to collector that don't fire
  analysisManager->FillNtupleDColumn(0, 9, fPlasticReflections); // Number of reflections off of the
                                                                // plastic box surface
  analysisManager->FillNtupleDColumn(0, 10, fPlasticAbsorptions); // Number of photons absorbed by the plastic
  analysisManager->FillNtupleDColumn(0, 11, fCopperReflections); // Number of photons reflected by copper
  analysisManager->FillNtupleDColumn(0, 12, fCopperAbsorptions); // Number of photons absorbed by copper
  analysisManager->FillNtupleDColumn(0, 13, fEscapedPhotons); // Number of photons that escape past the collector
  analysisManager->FillNtupleDColumn(0, 14, fScintillatorAbsorptions); // Number of photons that die in the scintillator
  analysisManager->FillNtupleDColumn(0, 15, fMuAbsorptions); // Number of photons that die in the mu metal
  analysisManager->FillNtupleDColumn(0, 16, fFoilAbsorptions); // Number of photons that die in the foil
  analysisManager->FillNtupleDColumn(0, 17, fFoilReflections); // Number of photons that die in the foil
  analysisManager->FillNtupleDColumn(0, 18, fcollectorreflections); // Number of photons that die in the foil
  G4cout << ">>> AnalysisManager says fConversionCoords.at(0) = " << fConversionCoords.at(0) << G4endl;
  analysisManager->FillNtupleDColumn(0, 19, fConversionCoords.at(0)); // x coordinate of photon conversion
  G4cout << ">>> AnalysisManager says fConversionCoords.at(1) = " << fConversionCoords.at(1) << G4endl;
  analysisManager->FillNtupleDColumn(0, 20, fConversionCoords.at(1)); // y coordinate of photon conversion
  G4cout << ">>> AnalysisManager says fConversionCoords.at(2) = " << fConversionCoords.at(2) << G4endl;
  analysisManager->FillNtupleDColumn(0, 21, fConversionCoords.at(2)); // z coordinate of photon conversion
  analysisManager->AddNtupleRow(0);
}

void TestSimAnalysisManager::Reset()
{
  G4cout << ">>> AnalysisManager is being reset" << G4endl;
  fPrimaryEnergy = 0.;
  fPrimaryTheta = 0.;
  fPrimaryPhi = 0.;
  fWLSabsorbed = 0;
  fNemitted = 0.;
  fcollectorhits = 0.;
  fcollectorduds = 0.;
  fPlasticReflections = 0.;
  fPlasticAbsorptions = 0.;
  fCopperReflections = 0.;
  fCopperAbsorptions = 0.;
  fEscapedPhotons = 0;
  fScintillatorAbsorptions = 0;
  fMuAbsorptions = 0;
  fFoilAbsorptions = 0;
  fFoilReflections = 0;
  fcollectorreflections = 0;
  fConversionCoords = {0,0,0};
}
