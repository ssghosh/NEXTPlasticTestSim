//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: TestSimDetectorConstruction.hh 69565 2013-05-08 12:35:31Z gcosmo $
//
/// \file TestSimDetectorConstruction.hh
/// \brief Definition of the TestSimDetectorConstruction class

#ifndef TestSimDetectorConstruction_h
#define TestSimDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class TestSimAnalysisManager;

/// Detector construction class to define materials and geometry.

class TestSimDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    TestSimDetectorConstruction(G4String geometry_spec, 
            G4double distance_spec,
            G4double teflon_spec,
            G4double mu_spec,
            G4double box_xy_spec,
            G4double box_length_spec,
            G4String device,
            G4String walls);
    virtual ~TestSimDetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    
    G4LogicalVolume* Getcollectorvolume() const { return fcollectorvolume; }
    G4LogicalVolume* GetPhotonCatcherVolume() const { return fPhotonCatcherVolume; }
    G4LogicalVolume* GetCopperVolume() const { return fCopperVolume; }
    G4LogicalVolume* GetMuVolume() const { return fMuVolume; }
    G4LogicalVolume* GetScintillatorVolume() const { return fScintillatorVolume; }
    G4LogicalVolume* GetGhostVolume() const { return fGhostVolume; }
    G4LogicalVolume* GetFoilVolume() const { return fFoilVolume; }
    G4LogicalVolume* GetTapeVolume() const { return fTapeVolume; }
    G4LogicalVolume* GetPlasticVolume() const { return fPlasticVolume; }
    G4LogicalVolume* GetWLSvolume() const { return fWLSvolume; }
    G4LogicalVolume* GetWorldVolume() const { return fWorldVolume; }
    G4LogicalVolume* GetMuBlocker() const { return fMuBlocker; }
    G4String         GetGeometry() const { return geometry; }
    G4double         GetDistance() const { return distance; } 
    G4String         GetWalls() const { return walls; } 

  protected:
    // always defined
    G4LogicalVolume*  fWorldVolume;
    G4LogicalVolume*  fcollectorvolume;
    G4LogicalVolume*  fCopperVolume;
    G4LogicalVolume*  fPhotonCatcherVolume;
    G4LogicalVolume*  fMuVolume;
    // scintillator geometry only
    G4LogicalVolume*  fGhostVolume;
    G4LogicalVolume*  fScintillatorVolume;
    G4LogicalVolume*  fFoilVolume;
    G4LogicalVolume*  fTapeVolume;
    // box geometry only
    G4LogicalVolume*  fPlasticVolume;
    G4LogicalVolume*  fWLSvolume;
    G4LogicalVolume*  fMuBlocker;
    // geometry specification (scintillator or box, distance)
    G4String          geometry;
    G4double          distance;
    G4double          mu_reflectivity;
    G4double          teflon_reflectivity;
    G4double          box_xy;
    G4double          box_length;
    G4String          device;
    G4String          walls;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

