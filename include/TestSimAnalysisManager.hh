//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file NEXTPlasticTestSim/include/TestSimAnalysisManager.hh
/// \brief Definition of the TestSimAnalysisManager class
//
//
// $Id: TestSimAnalysisManager.hh 99607 2018-08-23 13:33:42Z $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 

#ifndef TestSimAnalysisManager_h
#define TestSimAnalysisManager_h 1

#include "globals.hh"

#include "g4root.hh"
//#include "g4csv.hh"
//#include "g4xml.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class TestSimAnalysisManager
{
  public:
    TestSimAnalysisManager(G4double box_xy,
                           G4double box_length,
                           G4double tef_ref,
                           G4double mu_ref,
                           G4double ins,
                           G4String mat,
                           G4String led);
   ~TestSimAnalysisManager();

    void Book();
    void Save();

    void FillNtuple();
    void Reset();
    
    void Setcollectorinsertion(G4double d) { 
        std::cout << ">>> Setcollectorinsertion(" << d << ")" << std::endl;
        fcollectorinsertion = d; }
    void SetCopperRotation(G4double r) { 
        std::cout << ">>> SetCopperRotation(" << r << ")" << std::endl;
        fCopperRotation = r; }
    void SetPrimaryEnergy(G4double e) { 
        std::cout << ">>> SetPrimaryEnergy(" << e << ")" << std::endl;
        fPrimaryEnergy = e; }
    void SetPrimaryTheta(G4double t) { 
        std::cout << ">>> SetPrimaryTheta(" << t << ")" << std::endl;
        fPrimaryTheta = t; }
    void SetPrimaryPhi(G4double p) { 
        std::cout << ">>> SetPrimaryPhi(" << p << ")" << std::endl;
        fPrimaryPhi = p; }
    void SetPrimaryAbsorbed() { 
        std::cout << ">>> SetPrimaryAbsorbed()" << std::endl;
        fWLSabsorbed = 1; }
    void AddSecondaryPhoton() { 
        std::cout << ">>> AddSecondaryPhoton()" << std::endl;
        fNemitted += 1.0; }
    void Firecollector() { 
        std::cout << ">>> Firecollector()" << std::endl;
        fcollectorhits += 1.0; }
    void Misfirecollector() { 
        std::cout << ">>> Misfirecollector()" << std::endl;
        fcollectorduds += 1.0; }
    void Reflectedcollector() { 
        std::cout << ">>> Reflectedcollector()" << std::endl;
        fcollectorreflections += 1.0; }
    void CountPlasticReflection() { 
        std::cout << ">>> CountPlasticReflection()" << std::endl;
        fPlasticReflections += 1.0; }
    void CountPlasticAbsorption() { 
        std::cout << ">>> CountPlasticAbsorption()" << std::endl;
        fPlasticAbsorptions += 1.0; }
    void CountCopperReflection() { 
        std::cout << ">>> CountCopperReflection()" << std::endl;
        fCopperReflections += 1.0; }
    void CountCopperAbsorption() { 
        std::cout << ">>> CountCopperAbsorption()" << std::endl;
        fCopperAbsorptions += 1.0; }
    void CountEscapedPhoton() { 
        std::cout << ">>> CountEscapedPhoton()" << std::endl;
        fEscapedPhotons += 1.0; }
    void CountScintillatorAbsorption() { 
        std::cout << ">>> CountScintillatorAbsorption()" << std::endl;
        fScintillatorAbsorptions += 1.0; }
    void CountMuAbsorption() { 
        std::cout << ">>> CountMuAbsorption()" << std::endl;
        fMuAbsorptions += 1.0; }
    void CountFoilAbsorption() { 
        std::cout << ">>> CountFoilAbsorption()" << std::endl;
        fFoilAbsorptions += 1.0; }
    void CountFoilReflection() { 
        std::cout << ">>> CountFoilReflection()" << std::endl;
        fFoilReflections += 1.0; }
    void RecordConversionCoords(std::vector<G4double> coords) {
        fConversionCoords = coords;}

    void FillHisto(G4int id, G4double xbin, G4double ybin, G4double weight);
    void Normalize(G4int id, G4double fac);

  private:
    G4bool fFactoryOn;

    G4double    fBoxXY;
    G4double    fBoxLength;
    G4double    fTeflonReflectivity;
    G4double    fMuMetalReflectivity;
    G4double    fcollectorinsertion;
    G4String    fMaterial;
    G4String    fLED;
    G4double    fCopperRotation;
    G4double    fPrimaryEnergy;
    G4double    fPrimaryTheta;
    G4double    fPrimaryPhi;
    G4int       fWLSabsorbed;
    G4double    fNemitted;
    G4double    fcollectorhits;
    G4double    fcollectorduds;
    G4double    fcollectorreflections;
    G4double    fPlasticReflections;
    G4double    fPlasticAbsorptions;
    G4double    fCopperReflections;
    G4double    fCopperAbsorptions;
    G4double    fEscapedPhotons;
    G4double    fFoilReflections;
    G4double    fFoilAbsorptions;
    G4double    fMuAbsorptions;
    G4double    fScintillatorAbsorptions;
    std::vector<G4double> fConversionCoords;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
