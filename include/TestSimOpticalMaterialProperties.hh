#include <globals.hh>
#include <CLHEP/Units/SystemOfUnits.h>
#include <CLHEP/Units/PhysicalConstants.h>
//#include <SystemofUnits.h>
//#include <PhysicalConstants.h>

#include <CLHEP/Units/SystemOfUnits.h> // needed to get bar units
#include <CLHEP/Units/PhysicalConstants.h> // needed to get STP_Temperature

using namespace CLHEP;

class G4MaterialPropertiesTable;

class OpticalMaterialProperties
{
    public:
        static G4MaterialPropertiesTable* Air();
        static G4MaterialPropertiesTable* Teflon(G4double );
        static G4MaterialPropertiesTable* PMT();
        static G4MaterialPropertiesTable* SiPM();
        static G4MaterialPropertiesTable* Copper();
        static G4MaterialPropertiesTable* Mu(G4double );
        static G4MaterialPropertiesTable* Scintillator();
        static G4MaterialPropertiesTable* Foil();
        static G4MaterialPropertiesTable* Tape();
        static G4MaterialPropertiesTable* TPB();
        static G4MaterialPropertiesTable* TTX_TPB();
        static G4MaterialPropertiesTable* TM_TPB();
        static G4MaterialPropertiesTable* TTX();
        static G4MaterialPropertiesTable* TM();

    private:
        // Constructor (hidden)
        OpticalMaterialProperties();
        // Destructor (hidden)
        ~OpticalMaterialProperties();
};
