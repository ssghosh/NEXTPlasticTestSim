import matplotlib.pyplot as plt
import numpy as np
import csv
from scipy.optimize import minimize
from collections import namedtuple
plot_res = 300 #dpi

# some utility functions
def is_iterable(item):
    try:
        check = (e for e in item)
        return True
    except TypeError as te:
        #print(item,'is not iterable')
        return False

def normalize_data(data):
    dists,signals = data
    ref = signals[np.argmax(dists)]
    result = [dists,[signal/ref for signal in signals]]
    return result

def scale_data(data,multiplier):
    return [data[0],[multiplier*datum for datum in data[1]]]

            
def offset_data(data):
    dists,signals=data
    offset = dists[np.argmin(dists)]
    result = [[dist - offset for dist in dists],signals]
    return result

def read_sim(filename):
    sim = {}
    with open(filename,"r") as f:
        contents = f.readlines()
        percentage = ''
        insertions = []
        signals = []

        for line in contents:
            if len(line.split()) > 0 and line.split()[0] == 'Reflectivity:':
                if percentage != '':
                    sim[percentage] = [insertions,signals]
                    insertions = []
                    signals = []
                percentage = line.split()[1]
                continue
            elif len(line.split()) > 0 and line.split()[0] != 'Reflectivity':
                insertions.append(float(line.split()[0]))
                signals.append(float(line.split()[1]))
            else:
                continue
        # store last percentage data when you reach end of file
        sim[percentage] = [insertions,signals]
    return sim


def plot_sim(simdata,labelprefix,points=True,errbars=False,
             which_pcts=None):
    for key in simdata.keys():
        if which_pcts:
            if key not in which_pcts:
                continue
        if points and not errbars:
            plt.plot(simdata[key][0],simdata[key][1],
                     label=labelprefix+' '+key,
                     marker='',linestyle='--')
        elif points and errbars:
            plt.errorbar(simdata[key][0],simdata[key][1],
                    yerr=np.sqrt(simdata[key][1]),label=labelprefix+' '+key,
                    marker='.',linestyle='None')
        elif errbars and not points:
            plt.errorbar(simdata[key][0],simdata[key][1],
                    yerr=np.sqrt(simdata[key][1]),label=labelprefix+' '+key,
                    marker='None',linestyle='None')
        else:
            print("You've specified an impossible configuration!")
    plt.legend()

def sum_data(datasets):
    """
    Given a list of datasets (datasets, output of read_data), return a list of
    datasets in which all four SiPMs have been summed over.
    """
    summed_data = []
    summedSignals = []
    summedErrs = []
    for i in range(len(datasets)):
        summed = []
        err = []
        for dist in range(len(datasets[i]['Distance (mm)'])):
            dataset_sum = 0
            dataset_err = 0
            for nSIPM in range(1,5):
                dataset_sum += datasets[i]['Int '+str(nSIPM)+' (nWb)'][dist]
                # divide errs by 1000 since they're in pWb
                # append squared err since we're adding in quadrature
                dataset_err += (datasets[i]['Poisson err '+str(nSIPM)+' (nWb)'][dist])**2
            summed.append(dataset_sum)
            err.append(np.sqrt(dataset_err))
        summed_data.append({'Distance (mm)' : datasets[i]['Distance (mm)'],
                            'Int (nWb)' : summed,
                            'Poisson err (nWb)' : err})
    return summed_data

def normalize_for_biasv(data,vbias,vops):
    """
    Given a set of data, the value of the bias voltage
    used in the setup, and a list of the operating voltages (Vbr + 3V) for the
    SiPMs (this function assumes four SiPMs), returns a dataset in which all of
    the signals have been scaled such that they represent the gain the SiPM
    would have had at Vbr + 3V.
    """
    scaled_data = []
    for dataset in data:
        scaled_dataset = dataset
        for i_sipm in range(0,4):
            key = 'Int '+str(i_sipm+1)+' (nWb)'
            original_signals = dataset[key]
            scaled_signals = [(signal*3.)/(3.+vbias-vops[i_sipm]) for signal in dataset[key]]
            scaled_dataset[key] = scaled_signals
        scaled_data.append(scaled_dataset)
    return scaled_data

def read_csv(filename,diode='UV',vbias=None,vops=None,N=1000,summing_data=True):
    """
    Given a filename, the bias voltage used in the measurement, a list of the 
    operating voltages for each SiPM, the number of measurements that were
    taken to get the mean pulse area and its standard deviation, return
    a list of dictionaries containing each dataset in the file, a list of the
    conditions under which each of those datasets was taken ('coated', 'uncoated'),
    and a list of the thickness of the box for each measurement ('thinnest', 'medium',
    'thickest'). If summing_data is set to True, the list of dictionaries will
    consist of data summed over all four SiPMs.
    """
    numDataSets = 0

    def trim(myList):
        while myList[-1] == '':
            myList = myList[:len(myList)-1]
        return myList

    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False


    dataNamesAll = []
    dataPointsAll = []
    conditionsAll = []
    thicknessesAll = []
    dataDictAll = []
    syst1All = {}

    with open(filename, newline = '') as f:
        reader = list(csv.reader(f))
        for index, row in enumerate(reader):
            # Discover that we have a new curve
            if row[0] == 'SiPM':
                header = row
                config = reader[index+1]
                configurations = dict(zip(header, config))
                # Check to make sure we're looking at CM ready data
                if 'SKIP' in header:
                    continue
                elif (configurations['SiPM'] == 'Four blue' and configurations['Diode'] == diode
                        and 'SYST1' not in header and 'SYST2' not in header):
                    numDataSets += 1
                    thicknessesAll.append(configurations['Thickness'])
                    conditionsAll.append(configurations['Condition'])    
                    # Find the names of all the data types, and the data
                    scanCol = 0
                    dataNames = []
                    dataPoints = []
                    while reader[index+3][scanCol] != '':
                        dataNames.append(reader[index+3][scanCol])
                        # Find all the data points
                        scanRow = 0
                        dataVecCol = []
                        while is_number(reader[index+4+scanRow][scanCol]):
                            dataVecCol.append(float(reader[index+4+scanRow][scanCol]))
                            scanRow += 1
                        dataPoints.append(dataVecCol)
                        scanCol += 1
                    dataNamesAll.append(dataNames)
                    dataPointsAll.append(dataPoints)
                    dataDictAll.append(dict(zip(dataNames, dataPoints)))
                elif (configurations['SiPM'] == 'Four blue' #and configurations['Diode'] == diode
                        and 'SYST1' in header and 'SYST2' not in header):
                    currentkey = (configurations['Thickness'],configurations['Condition'])
                    systheader = reader[index+3]
                    systrow = reader[index+4]
                    systdict = dict(zip(systheader,systrow))
                    systerrs = []
                    for i_sipm in range(1,5):
                        systerrs.append(float(systdict['uncer '+str(i_sipm)]))
                    syst1All[currentkey] = systerrs
                else:
                    continue

    # Correct the "read distances" to "insertion distances"
    for i in range(numDataSets):
        offset = dataDictAll[i]['Distance (mm)'][np.argmin(dataDictAll[i]['Distance (mm)'])]
        for j in range(len(dataDictAll[i]['Distance (mm)'])):
            dataDictAll[i]['Distance (mm)'][j] -= offset

    # if given bias voltage and operating voltage information, normalize
    # everything to an overvoltage of 3V
    if vbias and vops:
        dataDictAll = normalize_for_biasv(dataDictAll,vbias,vops)

    # compute errors on the mean of the area measurement
    # (meas / sqrt(N))
    pwb_to_nwb = 1./1000. # since stdevs are given in pWb
    for i in range(len(dataDictAll)):
        for i_sipm in range(1,5):
            stdevs = dataDictAll[i]['Stdev '+str(i_sipm)+' (pWb)']
            errs_on_mean = [(elt*pwb_to_nwb)/np.sqrt(N) for elt in stdevs]
            dataDictAll[i]['Poisson err '+str(i_sipm)+' (nWb)'] = errs_on_mean

    # sum data and systs if needed
    if summing_data:
        dataDictAll = sum_data(dataDictAll)
        for key in syst1All.keys():
            systs = syst1All[key]
            systs = np.array(systs)
            quadrature = np.sqrt(np.sum(systs**2))
            syst1All[key] = quadrature
        

    return dataDictAll, conditionsAll, thicknessesAll, syst1All


def compute_chi2(alpha,dataset,single_MC,syst):
    """
    Given a scaling (alpha), a dataset (dataset, one element of the list of dictionaries
    produced by read_data), and an MC set (single_MC, one element from the dictionary
    produced by read_sim), return the reduced chi-squared of that ratio
    """
    single_MC = scale_data(single_MC,alpha)
    chi2 = 0
    if not syst:
        syst = 0
    if is_iterable(syst):
        syst = syst[0]


    # what are the keys for the columns with the integrals and errors?
    if 'Poisson err (nWb)' in dataset.keys():
        errkey = 'Poisson err (nWb)'
        intkey = 'Int (nWb)'
    else:
        print(">>> WARNING: This is unsummed data. Results will be for SiPM 1 ONLY")
        errkey = 'Poisson err 1 (nWb)'
        intkey = 'Int 1 (nWb)'

    for idx,dist in enumerate(dataset['Distance (mm)']):
        if dist not in single_MC[0]:
            continue
        else:
            total_uncert = np.sqrt(dataset[errkey][idx]**2 + syst**2)
            dist_idx = single_MC[0].index(dist)
            #chi2 += ((dataset[intkey][idx] - single_MC[1][dist_idx])/dataset[errkey][idx])**2
            chi2 += ((dataset[intkey][idx] - single_MC[1][dist_idx])/total_uncert)**2
    return chi2


def fit_ref_ratio(dataset,single_MC,initial_guess=0.01,syst=None):
    """
    Given a single dataset (one dict from the output of read_data) and
    a single set of MC (one value (a list of two lists) from the output of
    read_sim), return the multiplier for the MC curve which makes it best
    fit the dataset (float), and the chi^2 of that fit (float)
    """
    results = minimize(compute_chi2,initial_guess,
                        method='Nelder-Mead',
                        args=(dataset,single_MC,syst))
    ndf = len(dataset['Distance (mm)']) - 2
    chi2_over_ndf = results.fun/ndf
    return results.x[0], chi2_over_ndf


def fit_reflectivity(dataset,MC,syst=None):
    """
    Given a single dataset (one dict from the output of read_data) and
    a set of MC runs (the output of read_sim), return two objects: a tuple
    containing the best fit reflectance (string), the ratio by which that 
    reflectance's curve must be multiplied to fit (float), and the reduced
    chi^2 of the fit (float); and a dict with reflectances (strings) as keys,
    and tuples containing the best-fit ratio and reduced chi^2 for each of those
    reflectances.
    """
    ratios = dict.fromkeys(MC.keys())
    chi2s = dict.fromkeys(MC.keys())
    all_fits = dict.fromkeys(MC.keys())
    for key in MC.keys():
        ratio, chi2 = fit_ref_ratio(dataset,MC[key],syst=syst)
        ratios[key] = ratio
        chi2s[key] = chi2
        all_fits[key] = (ratio, chi2)

    fitted_val = min(chi2s,key=chi2s.get)
    fitted_ratio = ratios[fitted_val]
    fitted_chi2 = chi2s[fitted_val]
    best_fit_tuple = (fitted_val, fitted_ratio, fitted_chi2)
    return best_fit_tuple, all_fits

def fit_all_data(data,MC,systs=None,thicknesses=None,conditions=None):
    """
    Given data (the output of read_data) and MC (the output of read_sim),
    find the best fit reflectance to each dataset and return a list of the
    outputs of fit_reflectivity for each dataset.
    """
    fit_results = []
    all_fits_results = []

    for i in range(len(data)):
        syst = None
        if systs:
            syst = systs[(thicknesses[i],conditions[i])]
        fit_ref_result, all_fits = fit_reflectivity(data[i],MC,syst)
        fit_results.append(fit_ref_result)
        all_fits_results.append(all_fits)
    return fit_results, all_fits_results

def plot_fits(data,MC,fitresults,conditions,thicknesses,systs=None,xerr=1.,dataprefix='Data ',
        which_fits='best', which_conditions=None, which_thicknesses=None,
        label_contents=None):
    """
    Given a list of datasets (output of read_data), a set of MC (output of read_sim),
    a set of fit results for the data (second output of fit_all_data), a list of the conditions
    for each dataset, a list of the thicknesses for each dataset, a label prefix to 
    use for the data, whether to plot the best fit for each dataset ('best') or 
    all of the fits for each dataset ('all'), and optionally a list of conditions 
    ('coated', 'uncoated') and a list of thicknesses ('thinnest', 'medium', thickest')
    to plot, creates a plot of those datasets and their corresponding fits.
    """
    ax = plt.gca()
    # create a dict for markers and colors
    #markers = {'coated' : 'o',
    #           'uncoated' : '^'}
    markers = {'coated' : '',
               'uncoated' : ''}

    thicknesses = [elt.lower() for elt in thicknesses]
    unique_thicknesses = set(thicknesses)
    colors = {}
    for thickness in unique_thicknesses:
        color = next(ax._get_lines.prop_cycler)['color']
        colors[thickness] = color

    for idx, dataset in enumerate(data):
        if which_conditions:
            if conditions[idx] not in which_conditions:
                continue
        if which_thicknesses:
            if thicknesses[idx].lower() not in which_thicknesses:
                continue

        # plot the data
        sig_key = ''
        err_key = ''
        if 'Int (nWb)' in dataset.keys():
            sig_key = 'Int (nWb)'
            err_key = 'Poisson err (nWb)'
        else:
            print(">>> WARNING: This is unsummed data. Results will be for SiPM 1 ONLY")
            sig_key = 'Int 1 (nWb)'
            err_key = 'Poisson err 1 (nWb)'

        syst = 0
        if systs:
            syst = systs[(thicknesses[idx],conditions[idx])]
            if is_iterable(syst):
                syst = syst[0]

        errbars = [np.sqrt(err**2 + syst**2) for err in dataset[err_key]]
        data_label = ''
        if label_contents:
            for item in label_contents:
                if item == 'thickness':
                    data_label += thicknesses[idx]+' '
                elif item == 'condition':
                    data_label += conditions[idx]+' '
                elif item == 'led':
                    pass
                else:
                    print('>>> Unknwon label contents spec:',item)

        plt.errorbar(dataset['Distance (mm)'], dataset[sig_key], xerr=xerr,
                    #yerr=dataset[err_key], 
                    yerr=errbars, 
                    marker=markers[conditions[idx]],
                    linestyle='None',
                    #markersize=1,
                    color=colors[thicknesses[idx]],
                    label=dataprefix+data_label,
                    capsize=3)

        # colors correspond to thicknesses
        # markers correspond to coated/uncoated
        # plot the fit(s)
        if which_fits == 'best':
            all_fits = fitresults[idx]
            best_fit = min(all_fits.items(), key=lambda x: x[1][1])
            best_fit_ratio = best_fit[1][0]
            best_fit_chi2 = best_fit[1][1]
            best_fit_ref = best_fit[0]

            MC_scaled = scale_data(MC[best_fit_ref], best_fit_ratio)
            dist = dataset['Distance (mm)'][0]
            dist_idx = MC_scaled[0].index(dist)
            MC_label = r'MC fit '+best_fit_ref+', $\chi^{2}/\\nu=$'+format(best_fit_chi2,'.0f')
            MC_label = MC_label+', $\\alpha^{-1}=$'+format(1./best_fit_ratio,'.0f')
            plt.plot(MC_scaled[0], MC_scaled[1], 
                     label = MC_label,
                     marker='None', linestyle='--', color=colors[thicknesses[idx]])
        elif which_fits == 'all':
            all_fits = fitresults[idx]
            for key in all_fits.keys():
                fit_ratio = all_fits[key][0]
                fit_chi2 = all_fits[key][1]
                
                MC_scaled = scale_data(MC[key], fit_ratio)
                plt.plot(MC_scaled[0], MC_scaled[1], 
                        label=r'MC fit '+key+' '+conditions[idx]+' '+thicknesses[idx]+', $\chi^{2}/\\nu=$'+format(fit_chi2,'.0f')+', $\\alpha^{-1}=$'+format(1./fit_ratio,'.0f'),
                        marker='None', linestyle='--')
        elif not isinstance(which_fits,str):
            all_fits = fitresults[idx]
            for pct in which_fits:
                if pct in all_fits.keys():
                    fit_ratio = all_fits[pct][0]
                    fit_chi2 = all_fits[pct][1]
                    MC_scaled = scale_data(MC[pct], fit_ratio)
                    plt.plot(MC_scaled[0], MC_scaled[1], 
                            label=r'MC fit '+pct+', $\chi^{2}/\\nu=$'+format(fit_chi2,'.0f')+', $\\alpha^{-1}=$'+format(1./fit_ratio,'.0f'),
                            marker='None', linestyle='--')
                else:
                    print('>>> '+pct+' was not found within this MC set. Skipping...')
        else:
            print('>>> Unknown value for which_fits:',which_fits)
    handles, labels = ax.get_legend_handles_labels()
    if len(labels) > 0 and len(handles) > 0:
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    return labels,handles



def plot_data(data,conditions,thicknesses,systs=None,xerrs=None,which_conditions=None,
        which_thicknesses=False,ylims=None,label_contents=None):
    """
    Given all data (output of read_data), conditions and 
    thicknesses (output of read_data), plot all of the data. Optionally,
    specify the conditions (coated, uncoated) and thicknesses
    (thinnest, medium, thickest, 1cm) you want plotted. 
    """
    ax = plt.gca()
    # create a dict for markers and colors
    #markers = {'coated' : '.',
    #           'uncoated' : '.'}
    markers = {'coated' : '',
               'uncoated' : ''}

    thicknesses = [elt.lower() for elt in thicknesses]
    unique_thicknesses = set(thicknesses)
    colors = {}
    for thickness in unique_thicknesses:
        color = next(ax._get_lines.prop_cycler)['color']
        colors[thickness] = color


    for i in range(len(data)):
        if which_conditions:
            if conditions[i] not in which_conditions:
                continue
        if which_thicknesses:
            if thicknesses[i] not in which_thicknesses:
                continue

        int_idxs = []
        err_idxs = []
        if 'Poisson err (nWb)' in data[i].keys():
            err_idxs.append('Poisson err (nWb)')
            int_idxs.append('Int (nWb)')
        else:
            for i_sipm in range(1,5):
                err_idxs.append('Poisson err '+str(i_sipm)+' (nWb)')
                int_idxs.append('Int '+str(i_sipm)+' (nWb)')

        syst = [0]*len(err_idxs)
        if systs:
            syst = systs[(thicknesses[i],conditions[i])]
            if not is_iterable(syst):
                syst = [syst]
        print('>>> syst is',syst)

        for number, idxs in enumerate(zip(int_idxs,err_idxs)):
            if len(err_idxs) == 1:
                # the one scenario in which we don't want data
                # from the same thickness box to have the same color
                # is when we're doing coated/uncoated comparisons - so when
                # which_thicknesses >= 1
                if is_iterable(which_thicknesses) and len(which_thicknesses) >= 1:
                    point_color = next(ax._get_lines.prop_cycler)['color'] 
                else:
                    point_color = colors[thicknesses[i]]
                data_label = ''
                if label_contents:
                    data_label = ''
                    for item in label_contents:
                        if item == 'thickness':
                            data_label += thicknesses[i]+' '
                        elif item == 'condition':
                            data_label += conditions[i]+' '
                        elif item == 'led':
                            pass
                        else:
                            print('>>> Unknown label contents spec:',item)
            else:
                # in this case we know we're doing individual SiPMs
                data_label= 'SiPM '+str(number+1)
                point_color = next(ax._get_lines.prop_cycler)['color']

            #print('>>> syst[number] is',syst[number])
            err_idx = idxs[1]
            int_idx = idxs[0]
            syst_err = syst[number]
            #print('>>> data[i][err_idx] is',data[i][err_idx])
            #print('>>> syst_err is',syst_err)
            errorbars = [np.sqrt(err**2 + syst_err**2) for err in data[i][err_idx]]
            #print('>>> errorbars is',errorbars)


            #print('>>> plotting',thicknesses[i])
            ax.errorbar(data[i]['Distance (mm)'],data[i][int_idx],xerr=xerrs,
                     #yerr=data[i][err_idx],
                     yerr=errorbars,
                     label=data_label,
                     #markersize=1,
                     capsize=3,
                     color=point_color,marker=markers[conditions[i]],
                     linestyle='None')
        if ylims:
            plt.ylim(ylims)
    handles, labels = ax.get_legend_handles_labels()
    # sort both labels and handles by labels
    if len(labels) > 0 and len(handles) > 0:
        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    return labels,handles


# read in simulation data
uv_1mil_90s = read_sim('uv_1mil_90s.txt')
uv_1mil_allpcts = read_sim('uv_1mil_allpcts.txt')
blue_1mil_90s = read_sim('blue_1mil_90s.txt')
blue_1mil_allpcts = read_sim('blue_1mil_allpcts.txt')

#def plot_sim(simdata,labelprefix,points=True,errbars=False):
plt.figure()
plot_sim(blue_1mil_allpcts,'Blue A')
plt.legend()
plt.savefig('plots/blue_sim_comparison.png',dpi=300)
plt.close()

plt.figure()
plot_sim(uv_1mil_allpcts,'UV',which_pcts=['0.9','0.91','0.92','0.93','0.94',
    '0.95','0.96','0.97','0.98','0.99'])
plt.xlabel('Distance (mm)')
plt.ylabel('# of photons detected')
plt.title('Simulated UV LED reflectance curves')
plt.legend()
plt.savefig('plots/uv_sim_comparison.png',dpi=300)
plt.close()

# define the bias voltage used + vops for the four SiPMs
bias_v = 54.6 #V
v_ops = [54.98, 54.76, 54.52, 54.52]
# read in real data
alldata, conditions, thicknesses = read_csv('Reflectance_worksheet.csv',
                                            vbias = bias_v,
                                            vops = v_ops,
                                            summing_data = True)[0:3]
newdata, newconditions, newthicknesses = read_csv('Reflectance_worksheet_new.csv',
                                                  vbias = bias_v,
                                                  vops = v_ops,
                                                  summing_data = True)[0:3]

systdata, systconditions, systthicknesses, systerrs = read_csv('Reflectance_worksheet_systs.csv',
                                                      vbias = bias_v,
                                                      vops = v_ops,
                                                      summing_data = True)
#print('>>> systdata is',systdata)


# TODO: Plot of four SiPMs before overvoltage correction
# read in data without correcting or summing
uncorrected_results = read_csv('Reflectance_worksheet_systs.csv', summing_data = False)
uncorrected_data = uncorrected_results[0]
#print('>>> uncorrected_data is',uncorrected_data)
uncorrected_errs = uncorrected_results[3]
plt.figure()
plot_data(uncorrected_data, systconditions, systthicknesses, systs=uncorrected_errs, xerrs=1., 
          which_conditions=['coated'], which_thicknesses=['1 cm'],ylims=(35,65))
plt.legend()
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Individual signals before correction \n 1 cm coated with UV LED')
plt.savefig('plots/four_sipms_pre_correction.png',dpi=300)
plt.close()

# TODO: Plot of four SiPMs after overvoltage correction
corrected_results  = read_csv('Reflectance_worksheet_systs.csv', vbias = bias_v, 
                        vops = v_ops, summing_data=False)
corrected_data = corrected_results[0]
corrected_errs = corrected_results[3]
plt.figure()
plot_data(corrected_data, systconditions, systthicknesses, 
                         systs=corrected_errs,xerrs=1., 
                         which_conditions=['coated'],
                         which_thicknesses=['1 cm'],ylims=(35,65))
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Individual signals after correction \n 1 cm coated with UV LED')
plt.legend()
plt.savefig('plots/four_sipms_after_correction.png',dpi=300)
plt.close()


# TODO: Summed SiPM plot
plt.figure()
plot_data(systdata, systconditions, systthicknesses, xerrs=1., systs=systerrs,
          which_conditions=['coated'], which_thicknesses=['1 cm'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Summed after correction \n 1 cm coated with UV LED')
plt.legend()
plt.savefig('plots/after_summing.png',dpi=300)
plt.close()

# TODO: Plot with all uncoated thicknesses (UV)
plt.figure()
labels,handles=plot_data(systdata, systconditions, systthicknesses, systs=systerrs,xerrs=1., 
                        which_conditions=['uncoated'],label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Uncoated box comparison (UV LED)')
plt.legend(handles,labels)
plt.savefig('plots/all_uncoated_uv.png',dpi=300)
plt.close()

# TODO: Plot with all coated thicknesses (UV)
plt.figure()
labels,handles=plot_data(systdata, systconditions, systthicknesses, systs=systerrs,xerrs=1., 
          which_conditions=['coated'],label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Coated box comparison (UV LED)')
plt.legend(handles,labels)
plt.savefig('plots/all_coated_uv.png',dpi=300)
plt.close()

# TODO: Plots with coated/uncoated for each box
plt.figure()
plot_data(systdata, systconditions, systthicknesses, systs=systerrs,xerrs=1., 
        which_thicknesses=['0.5 cm'],label_contents=['condition'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('0.5 cm box coated/uncoated (UV LED)')
plt.legend()
plt.savefig('plots/coating_comparison_5mm.png',dpi=300)
plt.close()

plt.figure()
plot_data(systdata, systconditions, systthicknesses, systs=systerrs,xerrs=1., 
        which_thicknesses=['0.16 cm'],label_contents=['condition'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('0.16 cm box coated/uncoated (UV LED)')
plt.legend()
plt.savefig('plots/coating_comparison_1_6mm.png',dpi=300)
plt.close()

plt.figure()
plot_data(systdata, systconditions, systthicknesses, systs=systerrs,xerrs=1., 
        which_thicknesses=['0.24 cm'],label_contents=['condition'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('0.24 cm box coated/uncoated (UV LED)')
plt.legend()
plt.savefig('plots/coating_comparison_2_4mm.png',dpi=300)
plt.close()


plt.figure()
plot_data(systdata, systconditions, systthicknesses, systs=systerrs,xerrs=1., 
        which_thicknesses=['1 cm'],label_contents=['condition'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('1 cm box coated/uncoated (UV LED)')
plt.legend()
plt.savefig('plots/coating_comparison_10mm.png',dpi=300)
plt.close()


# TODO: Plot with all uncoated thicknesses (blue narrow)
bias_v = 54.6 #V
v_ops = [54.98, 54.76, 54.52, 54.52]
blue_a_data, blue_a_conditions, blue_a_thicknesses, blue_a_systs = read_csv(
            'Reflectance_worksheet_systs.csv',
            diode='Blue A',
            vbias=bias_v,
            vops=v_ops,
            summing_data=True)

blue_c_data, blue_c_conditions, blue_c_thicknesses, blue_c_systs = read_csv(
            'Reflectance_worksheet_systs.csv',
            diode='Blue C',
            vbias=bias_v,
            vops=v_ops,
            summing_data=True)

plt.figure()
plot_data(blue_a_data, blue_a_conditions, blue_a_thicknesses, systs=blue_a_systs,
          xerrs=1, which_conditions = ['uncoated'])
plt.legend()
plt.savefig('plots/blue_a_uncoated.png',dpi=300)
plt.close()
            
                                                                            

# TODO: Plot with all coated thicknesses (blue narrow)

# TODO: Plot with all uncoated thicknesses (blue wide)

# TODO: Plot with all coated thicknesses (blue wide)


allbestfits, allfits = fit_all_data(alldata,uv_1mil_90s)
allpctbestfits, allpctfits = fit_all_data(alldata,uv_1mil_allpcts)

newbestfits, newallfits = fit_all_data(newdata,uv_1mil_allpcts)
systfits, systallfits = fit_all_data(systdata,uv_1mil_allpcts,systs=systerrs,
                                     thicknesses=systthicknesses,
                                     conditions=systconditions)

# plot a fit
plt.figure()
plot_fits(systdata,uv_1mil_allpcts,systallfits,systconditions,systthicknesses,
         xerr=1., which_fits='best', 
         which_conditions=['uncoated'], 
         which_thicknesses=['0.16 cm'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Best fit to uncoated 0.16 cm box')
plt.legend()
plt.savefig('plots/uncoated_1_6mm_single_fit.png',dpi=300)
plt.close()

# Plot of what fit looks like for several reflectance values 
plt.figure()
plot_fits(systdata,uv_1mil_allpcts,systallfits,systconditions,systthicknesses,
         xerr=1., which_fits=['0.8','0.81','0.82','0.83','0.84','0.85','0.86',
                              '0.87','0.88','0.89','0.9'], 
         which_conditions=['uncoated'], 
         which_thicknesses=['0.16 cm'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('All fits to uncoated 0.16 cm box')
plt.legend(prop={'size' : 8})
plt.savefig('plots/all_test_fits.png',dpi=300)
plt.close()

# TODO: Plot of systematic error fits to all uncoated boxes (UV)
plt.figure()
labels,handles=plot_fits(systdata,uv_1mil_allpcts,systallfits,systconditions,systthicknesses,
         systs=systerrs,xerr=1., which_fits='best', which_conditions=['uncoated'],
         label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Reflectance fits to uncoated boxes (UV)')
plt.legend(handles,labels,prop={'size' : 7})
plt.savefig('plots/uv_uncoated_best_fits.png',dpi=300)
plt.close()

# TODO: Plot of systematic error fits to all coated boxes (UV)
plt.figure()
labels,handles=plot_fits(systdata,uv_1mil_allpcts,systallfits,systconditions,systthicknesses,
         systs=systerrs,xerr=1., which_fits='best', which_conditions=['coated'],
         label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Reflectance fits to coated boxes (UV)')
plt.legend(handles,labels,prop={'size' : 6})
plt.savefig('plots/uv_coated_best_fits.png',dpi=300)
plt.close()

# TODO: Plot of fits to all uncoated boxes (blue narrow)
blue_a_fits, blue_a_all_fits = fit_all_data(blue_a_data,blue_1mil_allpcts,systs=blue_a_systs,
                                     thicknesses=blue_a_thicknesses,
                                     conditions=blue_a_conditions)

plt.figure()
labels,handles=plot_fits(blue_a_data,blue_1mil_allpcts,blue_a_all_fits,blue_a_conditions,
          blue_a_thicknesses, systs=systerrs,xerr=1., which_fits='best', 
          which_conditions=['uncoated'],label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Reflectance fits to uncoated boxes (Blue A)')
plt.legend(handles,labels,prop={'size' : 8})
plt.savefig('plots/blue_a_uncoated_best_fits.png',dpi=300)
plt.close()

# TODO: Plot of fits to all coated boxes (blue narrow)
plt.figure()
labels,handles=plot_fits(blue_a_data,blue_1mil_allpcts,blue_a_all_fits,blue_a_conditions,
          blue_a_thicknesses, systs=systerrs,xerr=1., which_fits='best', 
          which_conditions=['coated'],label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Reflectance fits to coated boxes (Blue A)')
plt.legend(handles,labels,prop={'size' : 8})
plt.savefig('plots/blue_a_coated_best_fits.png',dpi=300)
plt.close()

# TODO: Plot of fits to all uncoated boxes (blue wide, use UV MC)
blue_c_fits, blue_c_all_fits = fit_all_data(blue_c_data,uv_1mil_allpcts,systs=blue_c_systs,
                                     thicknesses=blue_c_thicknesses,
                                     conditions=blue_c_conditions)
plt.figure()
labels,handles=plot_fits(blue_c_data,uv_1mil_allpcts,blue_c_all_fits,blue_c_conditions,
          blue_c_thicknesses, systs=systerrs,xerr=1., which_fits='best', 
          which_conditions=['uncoated'],label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Reflectance fits to uncoated boxes (Blue C)')
plt.legend(handles,labels,prop={'size' : 7})
plt.savefig('plots/blue_c_uncoated_best_fits.png',dpi=300)
plt.close()

# TODO: Plot of fits to all coated boxes (blue wide, use UV MC)
blue_c_fits, blue_c_all_fits = fit_all_data(blue_c_data,uv_1mil_allpcts,systs=blue_c_systs,
                                     thicknesses=blue_c_thicknesses,
                                     conditions=blue_c_conditions)
plt.figure()
labels,handles=plot_fits(blue_c_data,uv_1mil_allpcts,blue_c_all_fits,blue_c_conditions,
          blue_c_thicknesses, systs=systerrs,xerr=1., which_fits='best', 
          which_conditions=['coated'],label_contents=['thickness'])
plt.xlabel('Distance (mm)')
plt.ylabel('Pulse area (nWb)')
plt.title('Reflectance fits to coated boxes (Blue C)')
plt.legend(handles,labels,prop={'size' : 7})
plt.savefig('plots/blue_c_coated_best_fits.png',dpi=300)
plt.close()
