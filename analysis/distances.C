#include <iomanip>
#include <sstream>
#include <iostream>
#include <fstream>
#include <math.h>

void distances(std::string outputname) {
    gStyle->SetOptStat(0);

    std::vector<TString> fileList;

    // the template for the output filenames
    //TString nameTemplate = "box69x250_tefREFLECT_00_mu0_insINSERT.root";
    //TString nameTemplate = "box70x250_tefREFLECT_mu0_insINSERT_matPTFE.root";
    TString nameTemplate = "box70x250_tefREFLECT_mu0_insINSERT_matPTFE_leduv.root";

    // the reflectivities we're interested in
    std::vector<TString> reflectivities;
    //reflectivities.push_back("1.");
    //reflectivities.push_back("0.995");
    //reflectivities.push_back("0.999");
    //reflectivities.push_back("0.998");
    //reflectivities.push_back("0.997");
    //reflectivities.push_back("0.996");
    //reflectivities.push_back("0.994");
    //reflectivities.push_back("0.995");
    //reflectivities.push_back("0.993");
    //reflectivities.push_back("0.992");
    //reflectivities.push_back("0.991");
    //reflectivities.push_back("0.99");
    reflectivities.push_back("0.98");
    reflectivities.push_back("0.97");
    reflectivities.push_back("0.96");
    reflectivities.push_back("0.95");
    reflectivities.push_back("0.94");
    reflectivities.push_back("0.93");
    reflectivities.push_back("0.92");
    reflectivities.push_back("0.91");
    reflectivities.push_back("0.9");


    // these are the insertion distances, which the filenames contain
    std::vector<TString> distances;
    distances.push_back("216");
    distances.push_back("206");
    distances.push_back("196");
    distances.push_back("186");
    distances.push_back("156");
    distances.push_back("126");
    distances.push_back("96");
    distances.push_back("66");
    distances.push_back("36");
    distances.push_back("0");
    
    // create a vector of distances from the back of the box
    // using the insertion distances and the length of the box
    Int_t box_length = 250;
    std::vector<TString> distances_from_box;
    for (int i = 0; i < distances.size(); i++) {
        Int_t number = box_length - std::strtod(distances.at(i),NULL);
        distances_from_box.push_back(std::to_string(number));
    }
    
    // create an array of the distances from the box to use for TGraphErrors
    Int_t nDistances = distances.size();
    Double_t distances_array[nDistances];
    for (int i = 0; i < distances.size(); i++) {
        distances_array[i] = distances_from_box[i].Atoi();
    }

    // create a list of marker colors to differentiate the graphs
    std::vector<Color_t> colors;
    colors.push_back(kRed);
    colors.push_back(kBlue);
    colors.push_back(kGreen);
    colors.push_back(kOrange);
    colors.push_back(kBlack);
    colors.push_back(kCyan+1);

    // make a vector of TGraphErrors; each of these corresponds to a reflectivity
    //std::vector<TGraphErrors*> graphs;
    //// make a TMultiGraph to hold the graphs we'll be making
    //TString multiGraphTitle = "# of photons detected vs distance from back of box";
    //TCanvas *c1 = new TCanvas("c1");
    //TMultiGraph* multiGraph = new TMultiGraph();
    //multiGraph->GetXaxis()->SetTitle("Distance from back of box (mm)");
    //multiGraph->GetXaxis()->CenterTitle();
    //multiGraph->GetYaxis()->SetTitle("# of photons detected");
    //multiGraph->GetYaxis()->CenterTitle();
    //multiGraph->SetTitle(multiGraphTitle);
    //TLegend *legend = new TLegend(0.8,0.7,0.9,0.9);
    // make the output file
    ofstream outputfile;
    outputfile.open(outputname);
    for (int k = 0; k < reflectivities.size(); k++) {
        outputfile << "Reflectivity: " << reflectivities.at(k) << "\n";
        // Create arrays to hold counts and errors for this reflectivity
        Double_t counts[nDistances];
        Double_t errors[nDistances];

        // using the reflectivity and distance, construct the name of the file you need
        TString base = nameTemplate;
        base.Replace(13,7,reflectivities.at(k));


        for (int l = 0; l < distances.size(); l++) {
            
            outputfile << distances.at(l) << "\t";
            TString distance_base = base;
            Ssiz_t idx = distance_base.Index("INSERT",6,0,TString::kExact);
            distance_base.Replace(idx,6,distances.at(l));

            // open the file and find how many photons hit the PMT in this scenario
            TFile *file = new TFile(distance_base);
            TTree *tree = (TTree*)file->Get("ntuples/ntuple");

            Double_t nCollectorHits = 0;
            tree->SetBranchAddress("collectorhits",&nCollectorHits);

            Int_t nDetected = 0;

            for (int j = 0; j < tree->GetEntries(); j++) {
                tree->GetEntry(j);
                if (nCollectorHits > 0) nDetected++;
                if (nCollectorHits > 1) {
                    std::cout << "I found nCollectorHits > 1: " << nCollectorHits << std::endl;
                }
            }

            counts[l] = nDetected;
            outputfile << nDetected << "\n";
            errors[l] = sqrt(nDetected);
        }
        outputfile << "\n";
        
        // create a TGraphErrors for this reflectivity
        //TString graphName = reflectivities.at(k) + "%";
        //TString graphTitle = reflectivities.at(k) + "%";
        //TString outputname(base(0,base.Last('_'))+".png");


        //TCanvas *c2 = new TCanvas("c2");
        //TGraphErrors* graph = new TGraphErrors(nDistances, 
        //        distances_array, counts, 0, errors);
        //graph->SetName(graphName);
        //graph->SetTitle(graphTitle);
        //graph->SetMarkerColor(colors.at(k));
        //graph->SetLineColor(colors.at(k));
        //graph->SetMarkerStyle(20);
        //std::cout << ">>> fitting " << reflectivities.at(k) << "%..." << std::endl;
        ////graph->Fit("expo");
        //graph->Fit("pol3");
        //multiGraph->Add(graph);
        //gStyle->SetOptFit(1111);
        //graph->Draw("AP");
        //c2->Print(outputname);
        //gStyle->SetOptFit(0000);
        //graph->SetLineColor(colors.at(k));
        //graph->SetLineStyle(9);
        //graph->SetLineWidth(3);

        //graphs.push_back(new TGraphErrors(nDistances, distances_array, counts, distances_array_errs, errors));
        //graphs.at(k)->SetTitle(graphTitle);
        //graphs.at(k)->SetName(graphName);
        //graphs.at(k)->SetLineColor(colors.at(i));
        //graphs.at(k)->SetLineStyle(9);
        //graphs.at(k)->SetLineWidth(3);
        //graphs.at(k)->GetXaxis()->SetTitle("Distance from back of box (mm)");
        //graphs.at(k)->GetXaxis()->CenterTitle();
        //graphs.at(k)->GetYaxis()->SetTitle("# of photons detected");
        //graphs.at(k)->GetYaxis()->CenterTitle();
    }
    outputfile.close();

    

    // draw the multigraph and the legend

    //c1->cd();
    //gStyle->SetOptFit(0000);
    //c1->SetCanvasSize(900,700);
    ////c1->SetLogy();
    //multiGraph->Draw("AP");
    //c1->BuildLegend();
    //c1->Print("distances_reflectivity.png");

}
