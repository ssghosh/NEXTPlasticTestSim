#!/bin/bash

mu_ref=0.
box_xy=${1}
box_length=${2}
tef_ref=${3}
ins=${4}
mat=${5}
led=${6}

cd ../build
./TestSim run2.mac box ${ins} ${tef_ref} ${mu_ref} ${box_xy} ${box_length} SiPM ${mat} ${led}> box_out.txt
filename_material="${mat}"
filename_mu_ref=$(printf %.0f $(bc <<< "100*${mu_ref}"))
mv box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led}.root ../analysis/
#cp NEXTPlasticTestSim.root ../analysis/box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led}.root
cd ../analysis
mkdir box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led}
root -l -b -q 'report.C('"\"box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led}.root\"",70')' | tee box${box_xy}x${box_length}_tef${tef_ref}_mu${filename_mu_ref}_ins${ins}_mat${filename_material}_led${led}.txt
cd ../batch_submission
