#!/bin/bash

tef_reflectivities=(0.99 0.991 0.992 0.993 0.994 0.996 0.997 0.998 0.999)
#tef_reflectivities=(0.9 0.91 0.92 0.93 0.94 0.95 0.96 0.97 0.98)
insertions=(0 36 66 96 126 156 186 196 206 216)
#insertions=(0 36 66 96 126 156 186 216)
box_length=250
box_xy=70
materials=("PTFE")
leds=("blue" "uv")

for led in "${leds[@]}"
do
    echo "${led}"
    for mat in "${materials[@]}"
    do
        echo "${mat}"
        for tef_ref in "${tef_reflectivities[@]}"
        do
            echo "${tef_ref}"
            for ins in "${insertions[@]}"
            do
                echo "${ins}"
                sbatch batch_submit.sh ${box_xy} ${box_length} ${tef_ref} ${ins} ${mat} ${led}
            done
        done
    done
done
