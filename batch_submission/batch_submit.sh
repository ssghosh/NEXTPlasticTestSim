#!/bin/bash
#SBATCH -p serial_requeue # partition (queue)
#SBATCH -N 1 # number of nodes
#SBATCH -n 1 # number of cores
#SBATCH --mem 4000 # memory pool for all cores
#SBATCH -t 0-24:00 # time (D-HH:MM)
#SBATCH -o logs/slurm.%N.%j.out # STDOUT
#SBATCH -e logs/slurm.%N.%j.err # STDERR
module load cmake/3.5.2-fasrc01
module load root/6.14.00-fasrc01
source /n/holylfs02/LABS/guenette_lab/software/next/geant4/10.04.p03/bin/geant4.sh

./batch_launch_sim.sh ${1} ${2} ${3} ${4} ${5} ${6}
